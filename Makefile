ALL=cbn_deconstruction/tests.cmo cbv_construction/tests.cmo cbv_construction/machine.cmo invariant_derivation/tests.cmo

default: all

all: ${ALL}

common/term.cmo: common/term.ml
	ocamlc -c $^

common/trampoline.cmo: common/trampoline.ml
	ocamlc -c $^

cbn_deconstruction/tests.cmo: common/term.cmo cbn_deconstruction/tests.ml
	ocamlc -c -I common $^

cbv_construction/tests.cmo: common/term.cmo cbv_construction/tests.ml
	ocamlc -c -I common $^

cbv_construction/machine.cmo: common/term.cmo cbv_construction/tests.cmo common/trampoline.cmo cbv_construction/08_machine.ml
	ocamlc -c -I common -I cbv_construction $^ -o $@

invariant_derivation/tests.cmo: common/term.cmo invariant_derivation/tests.ml
	ocamlc -c -I common $^

clean:
	rm */*.cmi */*.cmo
