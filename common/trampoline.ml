
(* Trampolined style, as presented in the work of Ganz, Friedman and Wand,
 * can be used to divide computation into discrete steps explicitly.
 * We use this setting to iterate transition functions of our machines. *)

type 'a trampolined = Return of 'a | Bounce of (unit -> 'a trampolined);;

let rec pogo_stick (x:'a trampolined) : 'a =
  match x with
  | Return x' -> x'
  | Bounce x' -> pogo_stick @@ x'();;

let rec count_bounces (x:'a trampolined) : int =
  match x with
  | Return _  -> 0
  | Bounce x' -> 1 + (count_bounces @@ x'());;

(* trampolined is a monad *)

let rec trampolined_map (f: 'a -> 'b) : 'a trampolined -> 'b trampolined =
  function
  | Return x' -> Return (f x')
  | Bounce x' -> Bounce (fun () -> trampolined_map f @@ x'());;

let rec trampolined_join : 'a trampolined trampolined -> 'a trampolined =
  function
  | Return x' -> x'
  | Bounce x' -> Bounce (fun () ->  trampolined_join @@ x'());;

let trampolined_bind (x:'a trampolined) (f:'a -> 'b trampolined) : 'b trampolined =
   trampolined_join @@ trampolined_map f x;;

(* tools for optionals *)

let opt_try_step (f:'a -> 'a option) (x:'a) : 'a =
  match f x with
  | None    -> x
  | Some x' -> x';;
  
let rec opt_step_nf (f:'a -> 'a option) (x:'a) : 'a =
  match f x with
  | None    -> x
  | Some x' -> opt_step_nf f x';;


(* In a typical use, "opt_trampoline" takes a transition function
 * "trans" of an abstract machine,  and a configuration "x". It
 * simulates the machine step by step, i.e., one transition step
 * per bounce to the final configuration. *)

let rec opt_trampoline (trans:'a -> 'a option) (x:'a) : 'a trampolined =
  let rec_call = opt_trampoline trans in
  match trans x with
  | None    -> Return x
  | Some x' -> Bounce (fun () -> rec_call x');;

