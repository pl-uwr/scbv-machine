(* This file contains the definition of the language (lambda calculus)
   and a list of tests that are common for all normalization methods
   that we use. *)


(* Lambda terms in de Bruijn notation. Var(i) represents variable with
   de Bruijn index i (not to be confused with de Bruijn level). *)

type term = Var of int | App of term * term | Lam of term;;

let rec term_fold_right (var: int -> 'a) (app: 'a -> 'a -> 'a) (lam: 'a -> 'a)
  (t:term) : 'a =
  let rec_call = term_fold_right var app lam in
  match t with
  | Var x -> var x
  | App(u, v) -> app (rec_call u) (rec_call v)
  | Lam(e) -> lam (rec_call e);;

(* Pretty printer for lambda terms *)
let term_repr : term -> string = term_fold_right
  string_of_int
  (fun l r -> "("^l^" "^r^")")
  (fun e   -> "(λ"^e^")");;

(* Iteration is used in construction of complex examples of terms *) 
let rec iter (n:int) (f:'a -> 'a) (x:'a) : 'a =
  match n with
  | 0 -> x
  | _ -> iter (n-1) f (f x);;

(* Integer ranges are used to generate term families *)
let rec ints (b:int) (e:int) : int list =
  if b >= e then [] else b :: ints (b+1) e;;

(* Here we start building examples of lambda terms. The simplest one
   is the identity function. *)
let id : term = (Lam(Var 0));;

(* The following function is used to build from a list of terms a term
   consisting of a sequence of applications of the terms from the list *) 

let appseq (es:term list) : term =
  let rec appseq_aux (e:term) (es:term list) : term =
    match es with
    | [] -> e
    | e'::es' -> appseq_aux (App(e,e')) es' in
  match es with
  | [] -> id
  | e'::es' -> appseq_aux e' es';;


(* Crégut 2007 *)
let rec tau_lift (n:int) (i:int) : term -> term = function
  | Var m     -> Var (if m < i then m else m + n)
  | App(u, v) -> App(tau_lift n i u, tau_lift n i v)
  | Lam t     -> Lam(tau_lift n (i+1) t);;


(* More examples of lambda terms *) 
let yes = Lam(Lam(Var 1));;
let no  = Lam(Lam(Var 0));;

let const = yes;;

let zero = no;;

let church (n:int) : term = Lam( Lam( iter n (fun e -> App(Var 1, e)) (Var 0) ));;

let is_zero = Lam( appseq[Var 0; App(const, no); yes] );;

let succ = Lam( Lam( Lam( App(App(Var 2, Var 1), App(Var 1, Var 0)))));;

let addition = Lam( Lam( appseq[Var 1; succ; Var 0]));;

let multiplication = Lam( Lam( appseq[Var 1; (App(addition, Var 0)); zero]));;

let composition = Lam( Lam( Lam( App(Var 2, App(Var 1, Var 0)))));;

let koma = appseq [yes; no; yes; no; yes];;

let inert1 = Lam(appseq[Var 0; App(id, Var 0); Var 0]);;

let omega : term = Lam( App(Var 0, Var 0));;

let big_omega : term = App(omega, omega);;

let singleton_of (a:term) : term = Lam(App(Var 0, tau_lift 1 0 a));;

let pair_of (a:term) (b:term) : term = Lam(appseq[Var 0; tau_lift 1 0 a; tau_lift 1 0 b]);;

let pair : term = Lam(Lam(Lam(appseq[Var 0; Var 2; Var 1])));;

let dubleton : term = Lam(pair_of (Var 0) (Var 0));;

let fst : term = singleton_of yes;;

let snd : term = singleton_of no;;

let pred_aux : term = Lam(pair_of (App(snd, Var 0)) (App(succ,App(snd, Var 0))));;

let pred = Lam( appseq[Var 0; pred_aux; pair_of zero zero; yes]);;

let alpha = App( omega, Lam( Lam( App(Var 1, Var 0))));;

let alpha2 = Lam(App(Lam(App(Var 0, Lam(Var 1))), Var 0));;

let alpha3 = Lam(App(Lam(App(Var 0, Lam(Var 1))), App(Var 0, Var 0)));;

let example : term = App(Lam(Lam(Lam(App(Var 2,Var 1)))), Lam(App(Var 0, Var 0)));;

let y_comb : term = Lam(let t = Lam(App(Var 1,        App(Var 0, Var 0)        )) in App(t,t));;
let y_cbv  : term = Lam(let t = Lam(App(Var 1, Lam(appseq[Var 1; Var 1; Var 0]))) in App(t,t));;

let factorial_step     : term = Lam( Lam( appseq[is_zero;Var 0; church 1; appseq[composition; Var 0; App(Var 1, App(pred,Var 0))]] ));;
let factorial_step_cbv : term = Lam( Lam( appseq[is_zero;Var 0; church 1; appseq[composition; Var 0; Lam( App(App(Var 2, App(pred,Var 1)), Var 0))]] ));;

let factorial     = App(y_comb, factorial_step);;
let factorial_cbv = App(y_cbv,  factorial_step_cbv);;

(* à la Crégut 2007 *)
let test1 (n:int) : term = appseq [church n; church 2; id];;
let test2 (n:int) : term = App(pred, church n);;

let lambda_back : term = Lam(appseq[Var 0; Lam(Var 1); Var 0]);;

let explode (n:int) : term = Lam(appseq[church n; omega; Var 0]);;

let explode2 (n:int) : term = Lam(appseq[church n; Lam( App(Var 0, Lam (Var 1))); Var 0]);;

let inert_redex (n:int) : term = Lam(appseq[church n;
  Lam( App(Var 0, Lam (App(Lam (Var 2), Var 1))));
  Var 0]);;

let explode3 (n:int) : term = Lam(appseq[church n; dubleton; Var 0]);;

let explode4 (n:int) : term =     appseq[church n; dubleton; id];;

let contained_explosion (n:int) : term = appseq[church n; fst; (explode4 n)];;

(* A sequence of tests, parameterized by an evaluation method. We
   check if a given evaluation method correctly evaluate these terms
   to their normal forms. The expected result is a list consisting of
   constants "true". *)
let common_tests (eval : term -> term) : bool list = [
  eval succ = succ;
  eval alpha = Lam( Lam( App(Var 1, Var 0)));
  eval alpha2 = Lam(App(Var 0, Lam(Var 1)));
  eval alpha3 = Lam(App(App(Var 0, Var 0), Lam(App(Var 1, Var 1))));
  List.map (fun n -> eval @@ App(is_zero, church n)) @@ ints 0 5 = [yes;no;no;no;no];
  eval (appseq[addition;church 5;church 8]) = church 13;
  eval (appseq[multiplication;church 3;church 4]) = church 12;
  eval (appseq[composition;church 3;church 4]) = church 12;
  eval (appseq[church 4;church 4]) = church 256;
  eval (App(pred_aux, pair_of (church 5) (church 7))) = pair_of (church 7) (church 8);
  eval inert1 = Lam(appseq[Var 0; Var 0; Var 0]);
  eval koma = yes;
  eval example = Lam(Lam (App(Var 1, Var 1)));
  eval lambda_back = lambda_back;
  eval (explode2 2) = eval (inert_redex 2);
];;

(* normal forms and neutral terms *)
type nf      = Nf_lam of nf | Nf_neutral of neutral
 and neutral = N_var of int | N_app of neutral * nf

let rec term_of_nf : nf -> term = function
  | Nf_lam     e -> Lam(term_of_nf e)
  | Nf_neutral e -> term_of_neutral e
and term_of_neutral : neutral -> term = function
  | N_var n    -> Var n
  | N_app(u,v) -> App(term_of_neutral u, term_of_nf v);;
