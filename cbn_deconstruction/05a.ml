(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "05a.ml";;
*)

(* Now we are ready to refunctionalize the stack. We do it in two
   steps: first (here) we refunctionalize the type "stack1"; later (in
   the next step) we do the same with "stack2".

   We remove the datatype "stack1". To do this, we first look at the
   type of continuation function that consumes values of type
   "stack1" in '05_invariant.ml': 
      "continue1 : stack1 -> int -> value -> term" 
   This gives us the information that the datatype "stack1" is going
   to be replaced by the functional type "int -> value -> term". 

   Next, all calls to "continue1 s m v" are replaced with calls to 
   "s m v".

   Finally, in all places where elements of the type "stack1" were
   constructed, we replace the constructors with calls to a function
   obtained by inlining the "continue1" function:  
    "Clos(t2, e, s1)" with "fun m  v -> clos_cont t2 e s1 m v" in rule (1); 
   and "Normalize [...]" with  "fun m  v -> normalize [...]" in rules (9),(3) 
   and in the definition of "eval". 

   Then "continue1" is not used anymore, so we remove it. *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type stack2 = LAM of stack2 | Mark of term * (int -> value -> term) | ID
and
value = Abs of term * env | Neutral of term;;

(* datatype stack1 not needed anymore => transformed into functional
   type int -> value -> term as in type of the function continue1
   which consumes values of type stack1 *)

let rec run (t:tn) (e:env) (m:int) (s1: int -> value -> term) : term =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m @@  
                           (fun m  v -> clos_cont t2 e s1 m v) (* (1) *)
  | Term(Lam t)       -> s1 m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m s1
  | V(n)              -> s1 m @@ Neutral(Var(m-n))                   (* (6) *)
                       
and clos_cont (t':term) (e':env) (s1:int -> value -> term) (m:int) : value -> term = 
  function
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m s1         (* (2) *)
  | Neutral t -> run (Term t') e' m @@
                   (fun m v -> normalize (Mark(t, s1)) m v)         (* (8) *)

and normalize (s:stack2) (m:int) : value -> term = 
  function
  | Neutral t -> continue2 s m t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1)
                   (fun m v -> normalize (LAM s) m v)               (* (3) *)

(* continue1 disappeared => replaced by function application *)
               
and continue2 (s2:stack2) (m:int) (t:term) : term =
  match s2 with
  |           ID -> t                               (*  (7) *)
  |       LAM s2 -> continue2 s2 (m-1) @@ Lam t     (*  (9) *)
  | Mark(t1, s1) -> s1  m    @@ Neutral(App(t1, t)) (* (10) *);;

let eval (t:term) : term = run (Term t) [] 0 (fun m v -> normalize ID m v);;

let _ = tests eval;;

