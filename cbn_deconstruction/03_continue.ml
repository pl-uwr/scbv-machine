(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "03_continue.ml";;
*)

(*    We anticipate that "stack" in '02_disentanglement.ml' represents a 
 *    defunctionalized form of a continuation function, obtained from some 
 *    other program transformed  to continuation passing style. 
 *    Our goal is to refunctionalize this type, and we start by identifying 
 *    the continuation functions from the other program. Natural
 *    candidates are "run_l" and "run_c", as they operate on stacks.
 * 
 *    To make things easier, we prefer one continuation function instead
 *    of two. Looking at the types of the two functions
 *      "run_l: term -> env -> int -> stack -> term"
 *    and 
 *      "run_c: term        -> int -> stack -> term"
 *    we observe that they differ in the first two parameters, the rest
 *    is the same. Therefore we construct a new type (called "value" below), 
 *    which is the union of "term * env" and "term", and a new function 
 *    "continue" that behaves like "run_l" on elements from  "term * env" 
 *    and like "run_c" on elements from "term".
 * 
 *    It turns out that the new type will represent results of
 *    computation, hence the name "value". *)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type frame = Clos of term * env | LAM | Mark of term;;
type stack = frame list;;

type value = Abs of term * env | Normal of term;; (* note: "value" type is formed *)

let rec run (t:tn) (e:env) (m:int) (s:stack) : term =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m (Clos(t2, e)::s)    (* (1) *)
  | Term(Lam t)       -> continue s m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m s
  | V(n)              -> continue s m @@ Normal(Var(m-n))      (* (6) *)
and continue (s:stack) (m:int) (v:value) : term =
  match v with
  | Abs(t, e) ->(
    match s with
    | Clos(t',e')::s -> run (Term t) (Closure(Term t',e')::e) m s            (* (2) *)
    |              _ -> run (Term t) (Closure(V(m+1),[])::e) (m+1) (LAM::s)) (* (3) *) (* note: same continuation remains *)
  | Normal t ->
    match s with
    | Clos(t', e')::s -> run (Term t') e' m (Mark t::s)            (*  (8) *)
    |              [] -> t                                         (*  (7) *)
    |          LAM::s -> continue s (m-1) @@ Normal (Lam t)        (*  (9) *)
    |      Mark t'::s -> continue s  m    @@ Normal (App(t',t));;  (* (10) *)

let eval (t:term) : term = run (Term t) [] 0 [];;

let _ = tests eval;;

