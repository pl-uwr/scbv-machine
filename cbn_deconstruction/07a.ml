(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07a.ml";;
*)

(* There are two minor changes compared to "06b_direct.ml"
 * + "fun m ->" in the body of "run" is moved deeper in pattern matching
 * + the parameter of type "value" in the body of "normalize" is named "v"
 * *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;;

type name = int -> value;;

let rec run (t:tn) (e:env) : name =
  match t with
  | Term(App(t1, t2)) -> fun m -> apply_value (run (Term t1) e m) t2 e m  (* (1) *)
  | Term(Lam t)       -> fun _ -> Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in run t' e'  (* (4) & (5) *)
  | V(n)              -> fun m -> Neutral(Var(m-n))                       (* (6) *)
and apply_value (v:value) (t':term) (e':env) : name =
  fun m -> match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m            (* (2) *)
  | Neutral t -> Neutral(App(t, normalize m @@ run (Term t') e' m)) (* (10) & (8) *)
and normalize (m:int) (v:value) : term =
  match v with
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ run (Term t) (Closure(V(m+1), [])::e) (m+1));; (* (9) & (3) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [] 0;; (* (7) *)

let _ = tests eval;;

