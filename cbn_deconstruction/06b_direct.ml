(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "06b_direct.ml";;
*)

(* We are in direct style now. *)
(* Compared to "06a.ml" there are only a few minor changes: the type
   "name" is introduced; the function "clos_cont" changed its name to
   "apply_value"; functions "run" and "apply_value" are rewritten to
   use the new type. *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;;

type name = int -> value;; (* cf. call-by-name *)

let rec run (t:tn) (e:env) : name = (* note: (m:int) could be a 3rd parameter of run retruning a value *)
  fun m -> match t with
  | Term(App(t1, t2)) -> apply_value (run (Term t1) e m) t2 e m            (* (1) *)
  | Term(Lam t)       -> Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in run t' e' m (* (4) & (5) *)
  | V(n)              -> Neutral(Var(m-n))                                 (* (6) *)
and apply_value (v:value) (t':term) (e':env) : name =
  fun m -> match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m            (* (2) *)
  | Neutral t -> Neutral(App(t, normalize m @@ run (Term t') e' m)) (* (10) & (8) *)
and normalize (m:int) : value -> term =
  function
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ run (Term t) (Closure(V(m+1),[])::e) (m+1));; (* (9) & (3) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [] 0;; (* (7) *)

let _ = tests eval;;

