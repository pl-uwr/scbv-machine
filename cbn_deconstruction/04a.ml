(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "04a.ml";;
*)

(* A typical continue function uses a pattern matching on its first
   parameter (of the type that we just want to refunctionalize).

   The problem with the "continue" function from '03_continue.ml' is
   that it starts with pattern matching on value "v" (its third
   parameter) and only then it uses nested pattern matching that
   deconstructs the stack "s".  Here we change the order of pattern
   matching in "continue" such that it starts by deconstruction of the
   stack "s"; the nested pattern matching on "v" is moved to two new
   functions, "normalize" and "clos_cont". The name "normalize" comes
   from the observation that this function is used to normalize a body
   of a lambda abstraction.

   Inside the two new functions ("normalize" and "clos_cont") we
   identify fragments that behave like typical continue functions
   (i.e., fragments that destruct the stack). There is one such
   fragment inside "normalize" --- we separate it and move to a new
   function "continue2". *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type frame = Clos of term * env | LAM | Mark of term;;
type stack = frame list;;

type value = Abs of term * env | Normal of term;;

let rec run (t:tn) (e:env) (m:int) (s:stack) : term =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m @@ Clos(t2,e) ::s   (* (1) *)
  | Term(Lam t)       -> continue s m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m s
  | V(n)              -> continue s m @@ Normal(Var(m-n))      (* (6) *)
and clos_cont (t':term) (e':env) (s:stack) (m:int) : value -> term = (* note: behaviour of Clos frame depended on value constructor *)
  function
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m s (* (2) *)
  | Normal t -> run (Term t') e' m @@ (Mark t::s)          (* (8) *)
and normalize (s:stack) (m:int) : value -> term = (* note: behaviour of other frames was same for Abs values *)
  function
  | Normal t -> continue2 s m t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1) @@ (LAM :: s) (* (3) *)
and continue (s:stack) (m:int) (v:value) : term =
  match s with
  | Clos(t,e) ::s -> clos_cont t e s m v
  |             _ -> normalize s m v
and continue2 (s:stack) (m:int) (t:term) : term =
  match s with
  |           [] -> t                                      (*  (7) *)
  |     LAM :: s -> continue s (m-1) @@ Normal(Lam t)      (*  (9) *)
  | Mark t1 :: s -> continue s  m    @@ Normal(App(t1, t)) (* (10) *)
  | Clos(t,e) ::s -> assert false;;

let eval (t:term) : term = run (Term t) [] 0 [];;

let _ = tests eval;;

