(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "04b_invariant.ml";;
*)

(* In '04a.ml' we have an invariant that "continue2" is
   never called when the top of stack contains a closure. In fact, the
   invariant is more complicated and was already observed in [Cré2007,
   Lemma 4]: the content of a well-formed stack can be described by a
   regular expression. Note that finding such an invariant is a
   non-trivial task. The definition of the data type "stack1" below
   reflects the grammar of contexts corresponding to this regular
   expression.

   We also observe that we now have two continuation functions:
   "continue1" consuming results of type "value" and "continue2"
   consuming results of type "term".

   The constructor "Lam" does not go under "Normal" constructor of
   "value" type any more because "continue2" works on data of type "term"
   so its name "Normal" changes to "Neutral".

 *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type stack1 = Clos of term * env * stack1 | Normalize of stack2 (* note: possible stack space is narrowed *)
and stack2 = LAM of stack2 | Mark of term * stack1 | ID;;

type value = Abs of term * env | Neutral of term;; (* note: Normal changed to Neutral *)

let rec run (t:tn) (e:env) (m:int) (s1:stack1) : term =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m @@ Clos(t2,e,s1)    (* (1) *)
  | Term(Lam t)       -> continue1 s1 m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m s1
  | V(n)              -> continue1 s1 m @@ Neutral(Var(m-n))   (* (6) *)
and clos_cont (t':term) (e':env) (s1:stack1) (m:int) : value -> term =
  function
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m s1    (* (2) *)
  | Neutral t -> run (Term t') e' m @@ Normalize (Mark(t, s1)) (* (8) *)
and normalize (s:stack2) (m:int) : value -> term =
  function
  | Neutral t -> continue2 s m t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1) @@ Normalize (LAM s) (* (3) *)
and continue1 (s1:stack1) (m:int) (v:value) : term =
  match s1 with
  | Clos(t,e,s1) -> clos_cont t e s1 m v
  | Normalize k  -> normalize k m v
and continue2 (s2:stack2) (m:int) (t:term) : term =
  match s2 with
  |           ID -> t                                         (*  (7) *)
  |       LAM s2 -> continue2 s2 (m-1) @@ Lam t               (*  (9) *)
  | Mark(t1, s1) -> continue1 s1  m    @@ Neutral(App(t1, t)) (* (10) *);;

let eval (t:term) : term = run (Term t) [] 0 (Normalize ID);;

let _ = tests eval;;


