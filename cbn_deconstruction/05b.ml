(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "05b.ml";;
 *)

(* This is the second refunctionalization step. Here we remove the
   datatype "stack2". Again, we first look at the
   type of continuation function that consumes values of type "stack2"
   in '05a.ml':  
      "continue2 : stack2 -> int -> term -> term" 
   This gives us the information that the datatype "stack2" is going
   to be replaced by the functional type "int -> term -> term". 

   Again, all calls to "continue2 s m t" are replaced with calls to 
   "s m t".

   Again, in all places where elements of the type "stack2" were
   constructed, we replace the constructors with calls to a function
   obtained by inlining "continue2": "LAM s" is replaced with 
   "fun m t -> s (m-1) @@ Lam t"; "Mark(t1,s1)" is replaced with 
   "fun m t -> s1 m @@ Neutral(App(t1, t))"; "ID" is replaced with  
   "(fun m t -> t)".

   Then "continue2" is not used anymore, so we remove it. *)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;; 

(* datatype stack2 not needed anymore => transformed into functional
   type int -> term -> term as in type of the function continue2 which
   consumes values of type stack2 *)

let rec run (t : tn) (e : env) (m : int) (k1 : int -> value -> term) : term =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m @@
                           (fun m  v -> clos_cont t2 e k1 m v) (* (1) *)
  | Term(Lam t)       -> k1 m (Abs(t, e)) 
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m k1
  | V(n)              -> k1 m (Neutral(Var(m-n)))                    (* (6) *)
                       
and clos_cont (t' : term) (e' : env) (k1 : int -> value -> term) (m : int) (v: value) : term = 
  match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m k1         (* (2) *)
  | Neutral t -> run (Term t') e' m @@
                   (fun m v -> normalize (fun m n ->
                                   k1 m @@ (Neutral(App(t,n)))) m v) (* (8) & (10)*)

and normalize (s : int -> term -> term) (m:int) (v:value)  : term = 
  match v with
  | Neutral t -> s m t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1)
                   (fun m v -> normalize (fun m' t -> (* (3) *)
                                   s (m'-1) (Lam t)) m v)     (* (9) *)

(* continue2 disappeared => replaced by function application *)

let eval (t : term) : term =
  run (Term t) [] 0 (fun m v -> normalize (fun m n -> n) m v );; (* (7) *)

let _ = tests eval;;

