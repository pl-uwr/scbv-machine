(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "02_disentanglement.ml";;
*)

(* Here we disentangle recursion from '01_env_&_runs.ml' *)
(* Rules (2) and (3) in '01_env_&_runs.ml' are implemented with a
 * recursive function ("run_e"), where recursion depends on two
 * induction variables ("t" and "s") at the same time. We prefer 
 * recursive functions where recursion depends on a single variable, 
 * so we replace pattern matching on two variables 
 *   "match t,s with [...]"
 * with nested pattern matching on one variable 
 *   "match t with [...]  
 *    | match s with [...]"  
 * and then we introduce a new function "run_l" that implements the
 * inner matching *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type frame = Clos of term * env | LAM | Mark of term;;
type stack = frame list;;

let rec run_e (t:tn) (e:env) (m:int) (s:stack) : term =
  match t with
  | Term(App(t1, t2)) -> run_e (Term t1) e m (Clos(t2, e)::s)  (* (1) application evaluation *)
  | Term(Lam t)       -> run_l t e m s
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) envrionment examination *)
                         run_e t' e' m s
  | V(n)              -> run_c (Var(m-n)) m s                  (* (6) de Bruijn level interpretation *)
and run_l (t:term) (e:env) (m:int) (s:stack) : term =
  match s with
  | Clos(t',e')::s -> run_e (Term t) (Closure(Term t',e')::e) m s           (* (2) abstraction application *)
  |              _ -> run_e (Term t) (Closure(V(m+1),[])::e) (m+1) (LAM::s) (* (3) abstraction normalization *)
and run_c (t:term) (m:int) (s:stack) : term =
  match s with
  |              [] -> t                                (*  (7) return *)
  | Clos(t', e')::s -> run_e (Term t') e' m (Mark t::s) (*  (8) right application reconstruction *)
  |          LAM::s -> run_c (Lam t) (m-1) s            (*  (9) abstraction reconstruction *)
  |      Mark t'::s -> run_c (App(t',t)) m s;;          (* (10) left application reconstruction *)

let eval (t:term) : term = run_e (Term t) [] 0 [];;

let _ = tests eval;;

