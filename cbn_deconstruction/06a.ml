(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "06a.ml";;
*)

(* We apply the reverse  cps translation on the program from "05f_cps.ml" *)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;;

let rec run (t:tn) (e:env) (m:int) : value = 
  match t with
  | Term(App(t1, t2)) -> clos_cont (run (Term t1) e m) t2 e m            (* (1) *)
  | Term(Lam t)       -> Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in run t' e' m (* (4) & (5) *)
  | V(n)              -> Neutral(Var(m-n))                                 (* (6) *)
and clos_cont (v:value) (t':term) (e':env) (m:int) : value =
(* for clarity, order of arguments changed *)
  match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m            (* (2) *)
  | Neutral t -> Neutral(App(t, normalize m @@ run (Term t') e' m)) (* (10) & (8) *)
and normalize (m:int) : value -> term =
  function
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ run (Term t) (Closure(V(m+1),[])::e) (m+1));; (* (9) & (3) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [] 0;; (* (7) *)

let _ = tests eval;;

