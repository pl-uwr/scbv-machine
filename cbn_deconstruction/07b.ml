(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07b.ml";;
*)


(*    We anticipate that the type "Abs of term * env" is the result of
 *    defunctionalization of some other type; we want to refunctionalize
 *    it. Intuitively Abs(t,e) is a closure that should represent a
 *    function; since we are in call by name, the function should be of
 *    type name -> name. 

 *    In preparation for the refunctionalization we change the type of
 *    some values to names 
 *    + parameter "v" of "apply_value" is now of type "name" 
 *    + "apply_value" is renamed to "apply_name"
 *    + all calls to "apply_value" are changed accordingly: the parameter "v" 
 *      must now take additional argument to become a value
 *    Similar changes are made in "normalize".  *)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;;

type name = int -> value;;

let rec run (t:tn) (e:env) : name =
  match t with
  | Term(App(t1, t2)) -> fun m -> apply_name (run (Term t1) e) t2 e m   (* (1) *)
  | Term(Lam t)       -> fun _ -> Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in run t' e' (* (4) & (5) *)
  | V(n)              -> fun m -> Neutral(Var(m-n))                      (* (6) *)
and apply_name (v:name) (t':term) (e':env) : name =
  fun m -> match v m with
  | Abs(t, e)  -> run (Term t) (Closure(Term t',e')::e) m (* (2) *)
  | Neutral t -> Neutral(App(t, normalize m (run (Term t') e'))) (* (10) & (8) *)
and normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ run (Term t) (Closure(V(m+1), [])::e) );; (* (9) & (3) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [];; (* (7) *)

let _ = tests eval;;

