(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "01_env_&_runs.ml";;
*)

(*    Here we change notation from a term-rewriting one used in
 *    '00b_KN2020.ml' to a functional one. The type of configuration is
 *    removed; "E" configurations are replaced by the "run_e" function
 *    and "C" configurations are replaced by the "run_c" function. Each
 *    transition step (which is a rewriting of one configuration to
 *    another) is replaced by a call to appropriate function.
 * 
 *    There is one small optimization: rules (4) and (5) in
 *    '00b_KN2020.ml' implement selecting nth element from a list, so we
 *    replace it by a direct call to List.nth function.  *)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type frame = Clos of term * env | LAM | Mark of term;;
type stack = frame list;;

(* note: conf type is absent *)

let rec run_e (t:tn) (e:env) (m:int) (s:stack) : term =
  match t, s with
  | Term(App(t1, t2)),        _ -> run_e (Term t1) e m (Clos(t2, e)::s)                  (* (1) application evaluation *)
  | Term(Lam t), Clos(t',e')::s -> run_e (Term t) (Closure(Term t',e')::e) m s           (* (2) abstraction application *)
  | Term(Lam t),              _ -> run_e (Term t) (Closure(V(m+1),[])::e) (m+1) (LAM::s) (* (3) abstraction normalization *)
  | Term(Var n),              _ -> let Closure(t', e') = List.nth e n in                 (* (4) & (5) envrionment examination *)
                                   run_e t' e' m s
  | V(n),                     _ -> run_c (Var(m-n)) m s                                  (* (6) de Bruijn level interpretation *)
and run_c (t:term) (m:int) (s:stack) : term =
  match s with
  |              [] -> t                                (*  (7) return *)
  | Clos(t', e')::s -> run_e (Term t') e' m (Mark t::s) (*  (8) right application reconstruction *)
  |          LAM::s -> run_c (Lam t) (m-1) s            (*  (9) abstraction reconstruction *)
  |      Mark t'::s -> run_c (App(t',t)) m s;;          (* (10) left application reconstruction *)

let eval (t:term) : term = run_e (Term t) [] 0 [];;

let _ = tests eval;;

