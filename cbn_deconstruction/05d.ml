(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "05d.ml";;
*)

(* We want the "normalize" function to be in the image of the cps
   translation, where the first parameter ("s: int-> term->term") is a
   continuation. The resulting type of a continuation should be "'a",
   so we generalize "term" to "'a".

   The same kind of generalization happens to "clos_cont" and to
   "run".

   Then something less mechanical: we observe that the topmost
   continuation in the "eval" function, namely "fun m n -> n", does
   not depend on its first argument. By a manual control-flow analysis
   we checked that the continuation in the "normalize" function also
   does not depend on its first argument, so we remove this argument.

*)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;;

let rec run (t:tn) (e:env) (m:int) (k:int -> value -> 'a) : 'a =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m (clos_cont t2 e k)  (* (1) *)
  | Term(Lam t)       -> k m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m k
  | V(n)              -> k m @@ Neutral(Var(m-n))              (* (6) *)
and clos_cont (t':term) (e':env) (k:int -> value -> 'a) (m:int) (v:value) : 'a =
  match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m k                              (* (2) *)
  | Neutral t -> run (Term t') e' m (normalize (fun t' -> k m @@ Neutral (App(t, t')))) (* (8) & (10) *)
and normalize (k:term -> 'a) (m:int) : value -> 'a =
  function
  | Neutral t -> k t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1) (normalize (fun t -> k (Lam t)));; (* (3) & (9) *)

let eval (t:term) : term = run (Term t) [] 0 (normalize (fun t -> t));; (* (7) *)

let _ = tests eval;;

