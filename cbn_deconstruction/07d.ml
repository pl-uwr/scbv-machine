(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07d.ml";;
*)

(*    On our way to refunctionalize the "Abs of term * env" type, we want
 *    to have as few intermediate forms as possible. Here we get rid of
 *    "Closure of tn * env" type by its partial refunctionalization to
 *    the type "name" (which is a functional type int -> value). This
 *    propagates to a change of the "env" type from "closure list" to
 *    "name list".
 * 
 *    The function interpreting "Closure(t,n)" is "run".
 *    + inside "apply_abs" the type of "v" is changed from "closure" to "name"  
 *    + inside "normalize"  the constructor "Closure(V(m+1), [])" is replaced 
 *      with "run (V(m+1)) []", which is an application of the interpreting function
 *    + in "apply_name" the constructor "Closure(Term t',e')" is replaced with "v'", 
 *      and the arguments of the function are changed accordingly
 *    + inside "run" function, "Closure(t', e')" was constructed and immediately 
 *      destructed; this is simplified now.
 *
 *   The whole transformation is a partial refunctionalization because we do not 
 *   remove the interpreting function "run". *)



open Tests;;
open Term;;
type tn = Term of term | V of int;;

type value = Abs of term * env | Neutral of term
and name = int -> value
and env = name list;;

let rec run (t:tn) (e:env) : name =
  match t with
  | Term(App(t1, t2)) -> fun m -> apply_name (run (Term t1) e) (run (Term t2) e) m (* (1) *)
  | Term(Lam t)       -> fun _ -> Abs(t, e)
  | Term(Var n)       -> List.nth e n                                (* (4) & (5) *)
  | V(n)              -> fun m -> Neutral(Var(m-n))                  (* (6) *)
and apply_name (v:name) (v':name) : name =
  fun m -> match v m with
  | Abs(t, e) -> apply_abs t e v' m
  | Neutral t -> Neutral(App(t, normalize m v')) (* (10) & (8) *)
and normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ apply_abs t e (run (V(m+1)) [])) (* (9) & (3) *)
and apply_abs (t:term) (e:env) (v:name) : name = run (Term t) (v::e);; (* (2) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [];; (* (7) *)

let _ = tests eval;;

