(*
#directory "../common";;
#load_rec "tests.cmo";;
#load "../common/trampoline.cmo";;
#use "00b_KN2020.ml";;
*)

(* This machine is obtained from the original Crégut KN machine as
   implemented in '00a_KN2007.ml' by a few minor modifications. *)

(* The first modification is splitting the configurations "E(t,e,s,m)"
   into two kinds: those uses in the first six transitions, where the
   term "t" is decomposed and Evaluated, and those used in the last
   four transitions, where "t" is already in normal form and we
   Continue by analysing the content of the stack. *)

(* The second modification concerns the "lambda-nesting level" used in
   Crégut's formulation of the machine. It was incremented in
   transition 4.3 when an evaluation of a lambda abstraction starts,
   but it was never decremented. We expected that it should be
   decremented in 4.9, where an evaluation of a lambda abstraction
   finishes. So we add a decrement in this transition.

   In transition 4.8, KN2007 machine restores from the stack the
   lambda-nesting level of the currently processed lambda
   abstraction. But if we decrement this value as described above, we
   just have it, the stored value coincides with the current
   lamda-nesting level, and thus we do not need the stored
   value. Therefore we remove the integer from the type of marks.

   The third modification comes from the observation that the
   environment is never used in "C" configurations. It is either
   passed without modification to the next "C" configuration (as in
   4.9 and 4.10) or abandoned (as in 4.8).  Therefore we can safely
   remove it.

   We also change the order of arguments in "C" configurations and
   the order of "C" transitions to get a bit nicer code.

   In any case, the changes are so minor that the new machine strongly
   bisimulates the old one (it always evaluates a term in the same
   number of transitions of the same kind).*) 

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

(* Mark takes one argument instead of two *)
type frame = Clos of term * env | LAM | Mark of term;;
type stack = frame list;;

type conf = E of tn * env * int * stack | C of term * int * stack | D of term;;
(* Configuration of the form C does not need environment *)


let trans (c:conf) : conf option =
  match c with
  | E(Term(Var _), [],  _, _) -> None (* empty environment during examination (machine was given an open term) *)
  | D _                       -> None (* work is done *)
  | _ -> Some(
  match c with
  | E(Term(App(t1, t2)), e, m, s)           -> E(Term t1, e, m, Clos(t2, e)::s)               (*  (1) application evaluation *)
  | E(Term(Lam t), e, m,    Clos(t',e')::s) -> E(Term t, Closure(Term t',e')::e, m, s)        (*  (2) abstraction application *)
  | E(Term(Lam t), e, m,                 s) -> E(Term t,  Closure(V(m+1),[])::e, m+1, LAM::s) (*  (3) abstraction normalization *)
  | E(Term(Var 0), Closure(t, e)::_, m,  s) -> E(     t, e, m, s)                             (*  (4) envrionment examination *)
  | E(Term(Var n),             _::e, m,  s) -> E(Term (Var (n-1)), e, m, s)                   (*  (5) envrionment examination *)
  | E(V(n), e, m, s)                        -> C(     (Var (m-n)), m, s)                      (*  (6) de Bruijn level interpretation *)
  | C(     t, m,              [])           -> D t                                            (*  (7) return *)
  | C(     t, m, Clos(t', e')::s)           -> E(Term t', e', m, Mark t::s)                   (*  (8) right application reconstruction *)
  | C(     t, m,          LAM::s)           -> C(Lam t, m-1, s)                               (*  (9) abstraction reconstruction *)
  | C(     t, m,      Mark t'::s)           -> C(App(t', t), m, s)                            (* (10) left application reconstruction *)
  | E(Term(Var _), [], _, _) -> assert false
  | D _                      -> assert false);;

let load (t:term) : conf = E(Term t, [], 0, []);; (* initial configuration *)

let unload (c:conf) : term =
  match c with
  | D t -> t
  | _   -> assert false;;

open Trampoline;;

let nf_trampoline (t:term) : term trampolined = trampolined_map unload @@ opt_trampoline trans @@ load t;;

let steps_for_family (ts:int -> term) (n:int) : int = count_bounces @@ nf_trampoline @@ ts n;;

let eval (t:term) : term = pogo_stick @@ nf_trampoline t;;

let _ = [
  List.map (steps_for_family test1) @@ (ints 0 5 @ [9]) = [10; 25; 55; 115; 235; 7675];
  List.map (steps_for_family explode) @@ (ints 0 5 @ [9]) = [10; 22; 46; 94; 190; 6142]
] @ tests eval;;

