(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07i_nbe.ml";;
*)

(*    This is the final evaluator. It is an NbE (normalization by
 *    evaluation), full-reducing evaluator for lambda terms in de Bruijn
 *    notation, realizing the normal order reduction strategy.
 *
 *    We change names of functions to match NbE nomenclature:
 *    * eval is renamed to nbe
 *    * run is renamed to eval
 *    * normalize, after changing the order of arguments, is renamed to reify
 *    * apply_name is renamed to from_sem
 *    * lvar is inlined
 *    * the inlining of to_sem is reversed  *)

 open Tests;;
 open Term;;

(* semantic domain *)
type level = int
type glue  = Abs of (sem -> sem) | Neutral of term
 and sem   = level -> glue

(* reification of semantic objects into normal forms *)
let rec reify (d : sem) (m : level) : term =
  match d m with
  | Abs f ->
    Lam (reify (f (fun m' -> Neutral (Var (m'-m-1))))(m+1))
  | Neutral a ->
    a

(* sem -> sem as a retract of sem *)
let to_sem (f : sem -> sem) : sem =
  fun _ -> Abs f
let from_sem (d : sem) : sem -> sem =
  fun d' -> fun m ->
    match d m with
    | Abs f -> f d' m
    | Neutral a -> Neutral (App (a, reify d' m))

(* interpretation function *)
let rec eval (t : term) (e : sem list) : sem =
  match t with
  | Var n -> List.nth e n
  | Lam t' -> to_sem (fun d -> eval t' (d :: e))
  | App (t1, t2) -> from_sem (eval t1 e)
                             (fun m -> eval t2 e m)

(* NbE: interpretation followed by reification *)
let nbe (t : term) : term = reify (eval t []) 0

(* tests *)
let _ = tests nbe
