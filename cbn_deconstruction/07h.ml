(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07h.ml";;
*)


(* The functions "run", "apply_name" and "normalize" are no longer mutually 
 * recursive in "07g.ml" -- we reorganize them here. *)





open Tests;;
open Term;;

type value = Abs of (name -> name) | Neutral of term
and name = int -> value;;

let lvar (n:int) : name = fun m -> Neutral(Var(m-n));; (* (6) *)

let rec normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs f     -> Lam (normalize (m+1) @@ f (lvar (m+1)));; (* (9) & (3) *)

let apply_name (v:name) (v':name) : name =
  fun m -> match v m with
  | Abs f     -> f v' m
  | Neutral t -> Neutral(App(t, normalize m v'));; (* (10) & (8) *)

type env = name list;;

let rec run (t:term) (e:env) : name =
  match t with
  | App(t1, t2) -> fun m -> apply_name (run t1 e) (run t2 e) m (* (1) *)
  | Lam t       -> fun _ -> Abs(fun v -> run t (v::e))         (* (2) *)
  | Var n       -> List.nth e n                                (* (4) & (5) *)

let eval (t:term) : term = normalize 0 @@ run t [];; (* (7) *)

let _ = tests eval;;

