(*
#directory "../common";;
#load_rec "tests.cmo";;
#load "../common/trampoline.cmo";;
#use "00a_KN2007.ml";;
*)

open Tests;;
open Term;;

(* This file contains a direct implementation of the Crégut KN machine
   as presented on Table 4 in [Cré2007]. It is a version of Krivine
   machine (an abstract machine realizing call-by-name reduction
   strategy for lambda terms in de Bruijn notation) that reduces
   lambda terms to full normal form.  *)


(* V(n) represents an abstract variable. When a lambda abstraction is
   evaluated, a new bound variable is introduced to the environment;
   this new variable is identified by its de Bruijn level. *) 
(* Mark(t,n) is a special element that records a term (a portion of
   the normal form already built) and its lambda-nesting level *)
type tn = Term of term | V of int | Mrk of term * int ;;


type env = closure list
and closure = Closure of tn * env;;


type frame = Clos of term * env | LAM | Mark of term * int ;;
type stack = frame list;;

(*    There are two types of configurations of the KN machine. In an
 *    "E(t,e,s,m)" configuration  "t" is the currently processed term 
 *    and "e" is the current environment. Stack "s" contains (among 
 *    others) the arguments of "t", whose evaluation is delayed 
 *    (other elements of "s" will be discussed later). Number "n" 
 *    stores a lambda-nesting level.
 * 
 *    "D(t)" represents a final configuration, where the evaluation is
 *    done and "t" is the final result. *)

type conf = E of tn * env * stack * int | D of term;;


(* Let us briefly discuss the transitions of the KN machine presented
   below. The numbering of transitions comes from [Cré2007]. *)

(* In transition 4.1 an application of "t1" to "t2" is to be
 * evaluated. Since we are in call-by-name, we evaluate "t1" and
 * delay the evaluation of "t2" by storing it (together with the
 * current environment) on the stack. 
 * 
 * In 4.2, we are to evaluate a lambda abstraction "λt" that is going
 * to be applied to the topmost element "t'" from the stack; this is
 * done by creating a new binding in the environment (the variable
 * with de Bruijn index 0 is bound to "t'") and proceeding with the
 * evaluation of the body "t". *)

(* In 4.3 a lambda abstraction "λt" is to be evaluated, but it has
 * no argument on the stack. This is the place where strong
 * normalization comes to play. We are going to normalize the body
 * "t". To avoid confusion with arguments of "t" we push a LAM frame
 * to the stack - intuitively, it marks bottom of the stack from the
 * point of view of "t". Note that "t" may contain occurrances of a
 * variable bound by the current "λ". When we reach a point where we
 * want to evaluate this variable, the binding cannot be identified
 * by de Bruijn index (which is simply not present) and it must be
 * identified by the de Briun level of the abstraction in the final
 * result. This de Bruijn level, which is the current lambda-nesting 
 * level, is stored in the environment, and tha lambda-nesting level
 * is updated. *)

(* Transitions 4.4 and 4.5 implement a lookup in the
 * environment. Variable with de Bruijn index "n" is stored as the
 * "n"-th element of the environment. *) 

(* Transition 4.6 computes the de Bruijn index of an abstract
 * variable. At lambda-nesting level "m" a variable with de Bruijn
 * level "n" has de Bruijn index "m-n". *)

(* In the last four transitions the evaluation of "t" has  just 
 * finished, so "t" represents an already evaluated portion of the 
 * final result. *)

(* Transition 4.7 i s a final one, it just returns the result "t". *)

(* In 4.8, the evaluation of "t" has just finished, and the stack
 * contains an argument "t'" of "t" that should now be evaluated. We
 * continue with evaluation of "t'", keeping "t" on the top of stack
 * for a while. The constructor "Mark" prevents us from confusing "t"
 * with an argument of "t'". *)

(* In 4.9,  the evaluation of "t" has just finished, and the stack
 * contains neither an argument of "t" nor an information that "t" is
 * an argument of something else; it contains LAM, which marked bottom
 * of the stack from "t"'s point of view. We reconstruct the lambda
 * abstraction "λt" and continue. *) 

(* Transition 4.10 finishes the computation started in 4.8. We have 
 * finished the evaluation of the argument, so we pop the caller from 
 * the stack and reconstruct the application of the caller to the 
 * argument. *)


let trans (c:conf) : conf option =
  match c with
  | E(Term(Var _), [],  _, _) -> None (* empty environment during examination (machine was given an open term) *)
  | D _                       -> None (* work is done *)
  | _ -> Some(
  match c with
  | E(Term(App(t1, t2)), e, s, m)           -> E(Term t1, e, Clos(t2, e)::s, m)               (*  (4.1) application evaluation *)
  | E(Term(Lam t), e,    Clos(t',e')::s, m) -> E(Term t, Closure(Term t',e')::e, s, m)        (*  (4.2) abstraction application *)
  | E(Term(Lam t), e, s,                 m) -> E(Term t,  Closure(V(m+1),[])::e, LAM::s, m+1) (*  (4.3) abstraction normalization *)
  | E(Term(Var 0), Closure(t, e)::_, s,  m) -> E(     t, e, s, m)                             (*  (4.4) envrionment examination *)
  | E(Term(Var n),             _::e, s,  m) -> E(Term (Var (n-1)), e, s, m)                   (*  (4.5) envrionment examination *)
  | E(V(n), e, s, m)                        -> E(Mrk(Var(m-n),m), e, s, m)                    (*  (4.6) de Bruijn level interpretation *)
  | E(Mrk(t,_), _, [],              m)      -> D t                                            (*  (4.7) return *)
  | E(Mrk(t,n), e, Clos(t', e')::s, m)      -> E(Term t', e', Mark(t,n)::s, n)                (*  (4.8) right application reconstruction *)
  | E(Mrk(t,n), e, LAM::s,          m)      -> E(Mrk(Lam t,n), e, s, m)                       (*  (4.9) abstraction reconstruction *)
  | E(Mrk(t,n), e, Mark (t',n')::s, m)      -> E(Mrk(App(t', t), n'), e, s, m)                (* (4.10) left application reconstruction *)
  | E(Term(Var _), [], _, _) -> assert false
  | D _                      -> assert false);;

(* The "load" function creates an initial configuration for evaluating
 * a term. *)
let load (t:term) : conf = E(Term t, [], [], 0);; (* initial configuration *)

(* This restores a result from a final configuration. *)
let unload (c:conf) : term =
  match c with
  | D t -> t
  | _   -> assert false;;

open Trampoline;;

let nf_trampoline (t:term) : term trampolined = trampolined_map unload @@ opt_trampoline trans @@ load t;;

let steps_for_family (ts:int -> term) (n:int) : int = count_bounces @@ nf_trampoline @@ ts n;;

(* The evaluation function *) 
let eval (t:term) : term = pogo_stick @@ nf_trampoline t;;

(* Tests defined in "tests.ml" check wheter the evaluation function 
 * works correctly on several examples. The expected result is a list
 * consisting of constants "true".   *)

let _ = [
  List.map (steps_for_family test1) @@ (ints 0 5 @ [9]) = [10; 25; 55; 115; 235; 7675];
  List.map (steps_for_family explode) @@ (ints 0 5 @ [9]) = [10; 22; 46; 94; 190; 6142]
] @ tests eval;;

