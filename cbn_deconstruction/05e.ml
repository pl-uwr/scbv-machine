(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "05e.ml";;
*)

(* We propagate the observation from "05d". The continuation of
   "clos_cont" also does not depend on its first argument. *) 

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term;;

let rec run (t:tn) (e:env) (m:int) (k:int -> value -> 'a) : 'a =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m (clos_cont t2 e (k m))  (* (1) *)
  | Term(Lam t)       -> k m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m k
  | V(n)              -> k m @@ Neutral(Var(m-n))              (* (6) *)
and clos_cont (t':term) (e':env) (k: value -> 'a) (m:int) (v:value) : 'a =
  match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m (fun m n -> k n)                              (* (2) *)
  | Neutral t -> run (Term t') e' m @@  (fun m v -> normalize (fun n ->
                                   k @@ (Neutral(App(t,n)))) m v) (* (8) & (10)*)

and normalize (k:term -> 'a) (m:int) : value -> 'a =
  function
  | Neutral t -> k t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1) (normalize (fun t -> k (Lam t)));; (* (3) & (9) *)

let eval (t:term) : term = run (Term t) [] 0 (normalize (fun t -> t));; (* (7) *)

let _ = tests eval;;

