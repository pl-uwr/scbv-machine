(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07e.ml";;
 *)


(* We want to remove the type "V of int". In "07d.ml" it represents
   the function "fun m -> Neutral(Var(m-n))", so here we introduce the
   function (and we name it "lvar"). Inside "normalize" we replace 
   "V(m+1)" with "lvar(n+1)" and β-reduce. *)

open Tests;;
open Term;;
type tn = Term of term | V of int;;

type value = Abs of term * env | Neutral of term
and name = int -> value
and env = name list;;

let lvar (n:int) : name = fun m -> Neutral(Var(m-n));; (* (6) *)

let rec run (t:tn) (e:env) : name =
  match t with
  | Term(App(t1, t2)) -> fun m -> apply_name (run (Term t1) e) (run (Term t2) e) m (* (1) *)
  | Term(Lam t)       -> fun _ -> Abs(t, e)
  | Term(Var n)       -> List.nth e n                                (* (4) & (5) *)
  | V(n)              -> lvar(n)
and apply_name (v:name) (v':name) : name =
  fun m -> match v m with
  | Abs(t, e) -> apply_abs t e v' m
  | Neutral t -> Neutral(App(t, normalize m v')) (* (10) & (8) *)
and normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ apply_abs t e (lvar (m+1))) (* (9) & (3) *)
and apply_abs (t:term) (e:env) (v:name) : name = run (Term t) (v::e);; (* (2) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [];; (* (7) *)

let _ = tests eval;;

