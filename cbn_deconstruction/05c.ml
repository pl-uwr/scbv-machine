(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "05c.ml";;
*)

(* There are few minor changes to '05b.ml' here: 
 *   the function "clos_cont" in rule (1) is eta reduced; 
 *   continuations are now polymorphic, their return type is "'a";
 *   eta reduction in the second clause of clos_cont
 *   eta reduction in the second clause of normalize *)




open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

(* note: stack types are absent *)

type value = Abs of term * env | Neutral of term;;

let rec run (t:tn) (e:env) (m:int) (k:int -> value -> 'a) : 'a =
  match t with
  | Term(App(t1, t2)) -> run (Term t1) e m (clos_cont t2 e k)  (* (1) *)
  | Term(Lam t)       -> k m @@ Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in (* (4) & (5) *)
                         run t' e' m k
  | V(n)              -> k m @@ Neutral(Var(m-n))              (* (6) *)
and clos_cont (t':term) (e':env) (k:int -> value -> 'a) (m:int) (v:value) : 'a =
  match v with
  | Abs(t, e) -> run (Term t) (Closure(Term t',e')::e) m k                              (* (2) *)
  | Neutral t -> run (Term t') e' m @@ normalize (fun m t' -> k m @@ Neutral (App(t, t'))) (* (8) & (10) *)
and normalize (k:int -> term -> 'a) (m:int) : value -> 'a =
  function
  | Neutral t -> k m t
  | Abs(t, e) -> run (Term t) (Closure(V(m+1),[])::e) (m+1)
                   (normalize (fun m' t -> k (m'-1) (Lam t)));; (* (3) & (9) *)

let eval (t:term) : term = run (Term t) [] 0 (normalize (fun m t -> t));; (* (7) *)

let _ = tests eval;;

