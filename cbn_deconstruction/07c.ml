(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07c.ml";;
*)

(* In order to refunctionalize "Abs of term * env"  we have to
   identify a function that interprets elements of this type.  There
   is no direct candidate for such a function in 07b.ml, but we
   observe that both uses of "Abs(t,e)" in the definitions of
   "apply_name" and of "normalize" are quite similar --- they call
   "run" on "Term t" with environment "v::e" for some element v. This
   looks like the result of inlining a function "fun v -> run (Term t)
   (v::e)". So we reverse this inlining.  *)


open Tests;;
open Term;;
type tn = Term of term | V of int;;

type env = closure list
and closure = Closure of tn * env;;

type value = Abs of term * env | Neutral of term
and name = int -> value;;

let rec run (t:tn) (e:env) : name =
  match t with
  | Term(App(t1, t2)) -> fun m -> apply_name (run (Term t1) e) t2 e m   (* (1) *)
  | Term(Lam t)       -> fun _ -> Abs(t, e)
  | Term(Var n)       -> let Closure(t', e') = List.nth e n in run t' e' (* (4) & (5) *)
  | V(n)              -> fun m -> Neutral(Var(m-n))                      (* (6) *)
and apply_name (v:name) (t':term) (e':env) : name =
  fun m -> match v m with
  | Abs(t, e) -> apply_abs t e (Closure(Term t',e')) m
  | Neutral t -> Neutral(App(t, normalize m (run (Term t') e'))) (* (10) & (8) *)
and normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ apply_abs t e (Closure(V(m+1), []))) (* (9) & (3) *)
and apply_abs (t:term) (e:env) (v:closure) : name = run (Term t) (v::e);; (* (2) *)

let eval (t:term) : term = normalize 0 @@ run (Term t) [];; (* (7) *)

let _ = tests eval;;

