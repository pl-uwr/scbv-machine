open Term;;

let tests (eval : term -> term) : bool list = [
  eval (App(no,big_omega)) = id;
  eval (App(singleton_of big_omega, no)) = id;
  eval (Lam(App(factorial, church 5))) = Lam(church 120)
] @ common_tests eval;;

