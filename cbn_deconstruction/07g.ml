(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07g.ml";;
*)

(* We refunctionalize the datatype "Abs of term * env". We already know that the
 * function interpreting this type is
 *     apply_abs : term -> env -> name -> name
 * so the datatype "Abs of term * env" is going to be replaced by the functional 
 * type "name -> name". 

 * Next, all calls to "apply_abs t e v" are replaced with calls to "f v" 
 * where f is the function representing (t,e).  Finally, in all
 * places where elements of the type "Abs of term * env" were
 * constructed, we replace the constructors with calls to a function
 * obtained by inlining the "apply_abs" function.  *)


open Tests;;
open Term;;

type value = Abs of (name -> name) | Neutral of term
and name = int -> value
and env = name list;;

let lvar (n:int) : name = fun m -> Neutral(Var(m-n));; (* (6) *)

let rec run (t:term) (e:env) : name =
  match t with
  | App(t1, t2) -> fun m -> apply_name (run t1 e) (run t2 e) m (* (1) *)
  | Lam t       -> fun _ -> Abs(fun v -> run t (v::e))         (* (2) *)
  | Var n       -> List.nth e n                                (* (4) & (5) *)
and apply_name (v:name) (v':name) : name =
  fun m -> match v m with
  | Abs f -> f v' m
  | Neutral t -> Neutral(App(t, normalize m v')) (* (10) & (8) *)
and normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs f -> Lam (normalize (m+1) @@ f (lvar (m+1)));; (* (9) & (3) *)

let eval (t:term) : term = normalize 0 @@ run t [];; (* (7) *)

let _ = tests eval;;

