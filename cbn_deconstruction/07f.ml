(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07f.ml";;
*)

(*    In "07e.ml" the elements of the form "V(n)" are never constructed,
 *    so we may simply remove them.
 * 
 *    + the type "tn" now becomes "Term of term", which is isomorphic to 
 *      "term", so we replace it with "term".
 *    + the type of the first argument of "run" is changed from "tn" to "term" 
 *    + all constructors "Term" are removed  *)

open Tests;;
open Term;;

type value = Abs of term * env | Neutral of term
and name = int -> value
and env = name list;;

let lvar (n:int) : name = fun m -> Neutral(Var(m-n));; (* (6) *)

let rec run (t:term) (e:env) : name =
  match t with
  | App(t1, t2) -> fun m -> apply_name (run t1 e) (run t2 e) m (* (1) *)
  | Lam t       -> fun _ -> Abs(t, e)                          (* (2) *)
  | Var n       -> List.nth e n                                (* (4) & (5) *)
and apply_name (v:name) (v':name) : name =
  fun m -> match v m with
  | Abs(t, e) -> apply_abs t e v' m
  | Neutral t -> Neutral(App(t, normalize m v')) (* (10) & (8) *)
and normalize (m:int) (v:name) : term =
  match v m with
  | Neutral t -> t
  | Abs(t, e) -> Lam (normalize (m+1) @@ apply_abs t e (lvar (m+1))) (* (9) & (3) *)
and apply_abs (t:term) (e:env) (v:name) : name = run t (v::e);;

let eval (t:term) : term = normalize 0 @@ run t [];; (* (7) *)

let _ = tests eval;;

