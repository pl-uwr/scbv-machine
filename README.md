# An Abstract Machine for Strong Call by Value

This directory contains phases of program transformations aimed at
deconstruction of Crégut's KN machine and construction of its CbV variants.
The project is written in [OCaml](https://ocaml.org/) and tested in
OCaml version 4.07.0. Running any tests requires compiling it by `make`.

Program transformations are divided into two three:

* [cbn_deconstruction](cbn_deconstruction/) contains phases of deconstruction
  of a KN machine strongly bisimulating Crégut's KN from his journal work
  [Strongly reducing variants of the Krivine abstract machine](https://link.springer.com/article/10.1007%2Fs10990-007-9015-z),

* [cbv_construction](cbv_construction/) contains phases of construction of
  a call-by-value variant of KN,

* [invariant_derivation](invariant_derivation/) contains phases of construction of
  a call-by-value variant with explicit shape invariant.

Each of these three catalogues contains:

* A file containing definition of tests for given normalization method
  (e.g. [cbn_deconstruction/tests.ml](cbn_deconstruction/tests.ml)).

* Files of subsequent transformation phases (e.g. [cbn_deconstruction/00_KN2007.ml](cbn_deconstruction/00_KN2007.ml)).
  Name of each starts with two digits and potentially a letter defining the order.
  They are intended to facilitate working with an interactive
  OCaml console, which can be invoked with `ocaml`, or more conveniently
  on linux machines, with `OCAMLRUNPARAM=b ledit ocaml` command.

Each file of a transformation phase:

* starts with instructions for interactive OCaml console to load needed modules,

* contains a normalization method and

* calls tests shared for its catalogue.

Catalog [cbv_construction](cbv_construction/) contains also an example of term streaming
in file [cbv_construction/09_streaming.ml](cbv_construction/09_streaming.ml).

Remaining catalogues are:

* [common](common/) which contains type for standard representation of lambda
  expressions with de Bruijn indices, example expressions, list of tests for
  all normalization methods and tools for trampolining and

* [deps](deps/) with files depicting simplified view of program transformations
  as given below.

![repository outline](deps/deps.svg "Repository Outline")
