(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07_entanglement.ml";;
*)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type stack3 = ID | LAM of stack3 | LAPP of inert * stack2
 and stack2 = NEUT of stack3 | RAPP of nf * stack2;;

type stack1 = Normalize of stack3
  | Lapp of term * env * stack1 | Rapp of wnf * stack1;;

let rec run (t:term) (e:env) (s1:stack1) (m:int) : nf =
  match t with
  | App(t1, t2) -> run t2 e (Lapp(t1, e, s1)) m
  | Lam t       -> continue1 s1 (Abs(t, e)) m
  | Var n       -> continue1 s1 (List.nth e n) m
and continue1 (s1:stack1) (v:wnf) (m:int) : nf =
  match s1, v with
  | Lapp(t1, e, s1), _     -> run t1 e (Rapp(v, s1)) m
  | Rapp(v2, s1), Abs(t,e) -> run t (v2::e) s1 m
  | Rapp(v2, s1), Inert i  -> continue1 s1 (Inert(IApp(i, v2))) m
  | Normalize s3, Abs(t,e) -> run t (Inert(V(m+1))::e) (Normalize(LAM s3)) (m+1)
  | Normalize s3, Inert i  -> render_inert (NEUT s3) i m
and render_inert (s2:stack2) (i:inert) (m:int) : nf =
  match i with
  | V(n)       -> continue2 s2 (N_var(m-n)) m
  | IApp(i',w) -> continue1 (Normalize(LAPP(i', s2))) w m
and continue2 (s2:stack2) (t:neutral) (m:int) : nf =
  match s2 with
  | NEUT s3      -> continue3 s3 (Nf_neutral t) m
  | RAPP(t2, s2) -> continue2 s2 (N_app(t, t2)) m
and continue3 (s3:stack3) (t:nf) (m:int) : nf =
  match s3 with
  | ID          -> t
  | LAM s3      -> continue3 s3 (Nf_lam t) (m-1)
  | LAPP(i, s2) -> render_inert (RAPP(t, s2)) i m;;

let eval (t:term) : nf = run t [] (Normalize ID) 0;;

let _ = tests (fun t -> term_of_nf @@ eval t);;
