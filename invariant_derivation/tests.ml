open Term;;

let tests (eval : term -> term) : bool list = [
  eval (App(no,Lam(big_omega))) = id;
  eval (Lam(App(factorial_cbv, church 5))) = Lam(church 120);
  eval (pair_of (App(factorial_cbv, church 4)) (appseq [addition; church 2; church 2])) = pair_of (church 24) (church 4);
] @ common_tests eval;;

