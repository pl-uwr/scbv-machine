(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "02_closures.ml";;
*)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

let rec run (t:term) (e:env) : wnf =
  match t with
  | App(t1, t2) -> let v2 = run t2 e in apply_wnf (run t1 e) v2
  | Lam t       -> Abs(t, e)
  | Var n       -> List.nth e n
and apply_wnf (v:wnf) (v':wnf) : wnf =
  match v with
  | Abs(t,e) -> run t (v'::e)
  | Inert t  -> Inert(IApp(t, v'));;

let rec render_inert (i:inert) (m:int) : neutral =
  match i with
  | V(n)       -> N_var(m-n)
  | IApp(i',w) -> let t2 = normalize m w in N_app(render_inert i' m, t2)
and normalize (m:int) (v:wnf) : nf =
  match v with
  | Inert t  -> Nf_neutral (render_inert t m)
  | Abs(t,e) -> Nf_lam (normalize (m+1) @@ run t (Inert(V(m+1))::e));;

let eval (t:term) : nf = normalize 0 @@ run t [];;

let _ = tests (fun t -> term_of_nf @@ eval t);;
