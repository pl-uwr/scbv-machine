(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "03_cps.ml";;
*)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

let rec run (t:term) (e:env) (k:wnf -> 'a) : 'a =
  match t with
  | App(t1, t2) -> run t2 e (fun v2 ->
                   run t1 e (fun v1 ->
                   apply_wnf v1 v2 k))
  | Lam t       -> k @@ Abs(t, e)
  | Var n       -> k @@ List.nth e n
and apply_wnf (v:wnf) (v': wnf) (k:wnf -> 'a) : 'a =
  match v with
  | Abs(t,e) -> run t (v'::e) k
  | Inert t  -> k @@ Inert(IApp(t, v'));;

let rec render_inert (i:inert) (m:int) (k:neutral -> 'a) : 'a =
  match i with
  | V(n)       -> k @@ N_var(m-n)
  | IApp(i',w) -> normalize m w (fun t2 -> 
                  render_inert i' m (fun t1 ->
                  k @@ N_app(t1, t2)))
and normalize (m:int) (v:wnf) (k:nf -> 'a) : 'a =
  match v with
  | Inert t  -> render_inert t m (fun n -> k @@ Nf_neutral n)
  | Abs(t,e) -> run t (Inert(V(m+1))::e) (fun v ->
                normalize (m+1) v (fun t -> k @@ Nf_lam t));;

let eval (t:term) : nf = run t [] (fun v -> normalize 0 v (fun t -> t));;

let _ = tests (fun t -> term_of_nf @@ eval t);;
