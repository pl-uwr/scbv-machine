(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "04c_invariant.ml";;
*)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type stack3 = ID | LAM of stack3 | LAPP of inert * int * stack2
 and stack2 = NEUT of stack3 | RAPP of nf * stack2;;

type stack1 = Normalize of int * stack3
  | Lapp of term * env * stack1 | Rapp of wnf * stack1;;

let rec run (t:term) (e:env) (s1:stack1) : nf =
  match t with
  | App(t1, t2) -> run t2 e @@ Lapp(t1, e, s1)
  | Lam t       -> continue1 s1 @@ Abs(t, e)
  | Var n       -> continue1 s1 @@ List.nth e n
and apply_wnf (v:wnf) (v': wnf) (s1:stack1) : nf =
  match v with
  | Abs(t,e) -> run t (v'::e) s1
  | Inert t  -> continue1 s1 @@ Inert(IApp(t, v'))
and render_inert (i:inert) (m:int) (s2:stack2) : nf =
  match i with
  | V(n)       -> continue2 s2 @@ N_var(m-n)
  | IApp(i',w) -> normalize m w @@ LAPP(i', m, s2)
and normalize (m:int) (v:wnf) (s3:stack3) : nf =
  match v with
  | Inert t  -> render_inert t m @@ NEUT s3
  | Abs(t,e) -> run t (Inert(V(m+1))::e) @@ Normalize(m+1, LAM s3)
and continue1 (s1:stack1) (v:wnf) : nf =
  match s1 with
  | Normalize(m, s3) -> normalize m v s3
  | Lapp(t1, e, s1)  -> run t1 e @@ Rapp(v, s1)
  | Rapp(v2, s1)     -> apply_wnf v v2 s1
and continue2 (s2:stack2) (t:neutral) : nf =
  match s2 with
  | NEUT s3      -> continue3 s3 @@ Nf_neutral t
  | RAPP(t2, s2) -> continue2 s2 @@ N_app(t, t2) 
and continue3 (s3:stack3) (t:nf) : nf =
  match s3 with
  | ID             -> t
  | LAM s3         -> continue3 s3 @@ Nf_lam t
  | LAPP(i, m, s2) -> render_inert i m @@ RAPP(t, s2);;

let eval (t:term) : nf = run t [] @@ Normalize(0, ID);;

let _ = tests (fun t -> term_of_nf @@ eval t);;
