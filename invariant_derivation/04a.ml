(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "04a.ml";;
*)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type stack3 = ID | LAM of stack3 | LAPP of inert * int * (neutral -> nf);;

let rec run (t:term) (e:env) (k:wnf -> nf) : nf =
  match t with
  | App(t1, t2) -> run t2 e (fun v2 ->
                   run t1 e (fun v1 ->
                   apply_wnf v1 v2 k))
  | Lam t       -> k @@ Abs(t, e)
  | Var n       -> k @@ List.nth e n
and apply_wnf (v:wnf) (v': wnf) (k:wnf -> nf) : nf =
  match v with
  | Abs(t,e) -> run t (v'::e) k
  | Inert t  -> k @@ Inert(IApp(t, v'));;

let rec render_inert (i:inert) (m:int) (k:neutral -> nf) : nf =
  match i with
  | V(n)       -> k @@ N_var(m-n)
  | IApp(i',w) -> normalize m w @@ LAPP(i', m, k)
and normalize (m:int) (v:wnf) (s3:stack3) : nf =
  match v with
  | Inert t  -> render_inert t m (fun n -> continue3 s3 @@ Nf_neutral n)
  | Abs(t,e) -> run t (Inert(V(m+1))::e) (fun v ->
                normalize (m+1) v @@ LAM s3)
and continue3 (s3:stack3) (t:nf) : nf =
  match s3 with
  | ID             -> t
  | LAM s3         -> continue3 s3 @@ Nf_lam t
  | LAPP(i, m, k2) -> render_inert i m (fun t1 -> k2 @@ N_app(t1, t));;

let eval (t:term) : nf = run t [] (fun v -> normalize 0 v ID);;

let _ = tests (fun t -> term_of_nf @@ eval t);;
