(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "01_inert.ml";;
*)

open Tests;;
open Term;;

type value = Abs of (value -> value) | Inert of inert
and inert = V of int | IApp of inert * value;;

let rec render_inert (i : inert) (m : int) : neutral =
  match i with
  | V(n)       -> N_var(m-n)
  | IApp(i',w) -> let t2 = normalize m w in N_app(render_inert i' m, t2)
and normalize (m : int) (d : value) : nf =
  match d with
  | Inert l -> Nf_neutral (render_inert l m)
  | Abs f   -> Nf_lam (normalize (m+1) @@ f (Inert(V(m+1))));;

let apply_value (d : value) (d' : value) : value =
  match d with
  | Abs f   -> f d'
  | Inert l -> Inert (IApp(l, d'));;

type env = value list;;

let rec run (t : term) (e : env) : value =
  match t with
  | Var n -> List.nth e n
  | App (t1, t2) -> let d2 = run t2 e
                    in apply_value (run t1 e) d2
  | Lam t' -> Abs (fun d -> run t' (d :: e));;

let eval (t : term) : nf = normalize 0 (run t []);;

let _ = tests (fun t -> term_of_nf @@ eval t);;
