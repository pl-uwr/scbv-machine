(*
#directory "../common";;
#load_rec "tests.cmo";;
#load "../common/trampoline.cmo";;
#use "08_machine.ml";;
*)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type stack3 = ID | LAM of stack3 | LAPP of inert * stack2
 and stack2 = NEUT of stack3 | RAPP of nf * stack2;;

type stack1 = Normalize of stack3
  | Lapp of term * env * stack1 | Rapp of wnf * stack1;;

type conf = E of term * env * stack1 * int
  | C1 of stack1 * wnf * int | C2 of stack2 * neutral * int
  | C3 of stack3 *  nf * int | RI of stack2 *   inert * int;;

  let trans (c:conf) : conf option =
    match c with
    | E(Var _, [], _, _)     -> None
    | C3(ID, t, _)           -> None
    | _ -> Some(
    match c with
    | E(App(t1, t2), e, s1, m) -> E(t2, e, (Lapp(t1, e, s1)), m)
    | E(Lam t, e, s1, m)       -> C1(s1, Abs(t,e), m)
    | E(Var 0, w::_, s1, m)    -> C1(s1, w, m)
    | E(Var n, _::e, s1, m)    -> E(Var(n-1), e, s1, m)
    | C1(Lapp(t, e, s1), w, m)      -> E(t, e, Rapp(w, s1), m)
    | C1(Rapp(w, s1), Abs(t,e), m)  -> E(t, w::e, s1, m)
    | C1(Rapp(w, s1), Inert i,  m)  -> C1(s1, Inert(IApp(i, w)), m)
    | C1(Normalize s3, Abs(t,e), m) -> E(t, Inert(V(m+1))::e, Normalize(LAM s3), m+1)
    | RI(s2, IApp(i,w), m)          -> C1(Normalize(LAPP(i, s2)), w, m)
    | RI(s2, V(n), m)               -> C2(s2, N_var(m-n), m)
    | C3(LAPP(i, s2), t_nf, m)     -> RI(RAPP(t_nf, s2), i, m)
    | C3(LAM s3, t_nf, m)          -> C3(s3, Nf_lam t_nf, m-1)
    | C2(RAPP(t_nf, s2), t_neu, m) -> C2(s2, N_app(t_neu, t_nf), m)
    | C1(Normalize s3, Inert i, m) -> RI(NEUT s3, i, m)
    | C2(NEUT s3, t_neu, m)        -> C3(s3, Nf_neutral t_neu, m)
    | E(Var _, [], _, _)     -> assert false
    | C3(ID, t, _)           -> assert false
    );;
  
  let load (t:term) : conf = E(t, [], Normalize ID, 0);;
  
  let unload (c:conf) : nf =
    match c with
    | C3(ID, t, _) -> t
    | _            -> assert false;;
  
  open Trampoline;;
  
  let nf_trampoline (t:term) : nf trampolined = trampolined_map unload @@ opt_trampoline trans @@ load t;;
  
  let steps_for_family (ts:int -> term) (n:int) : int = count_bounces @@ nf_trampoline @@ ts n;;
  
  let eval (t:term) : nf = pogo_stick @@ nf_trampoline t;;
  
  let ith_conf_of (i:int) (t:term) : conf = iter i (opt_try_step trans) (load t);;
  
  let same_confs_of (i:int) (j:int) (t:term) : bool = (ith_conf_of i t = ith_conf_of j t);;
  
  let _ = [
    List.map (steps_for_family test1) @@ (ints 0 5 @ [9]) = [17; 35; 65; 119; 221; 6203];
    List.map (steps_for_family explode) @@ (ints 0 5 @ [9]) = [17; 33; 55; 89; 147; 3173];
    List.map (steps_for_family contained_explosion) @@ (ints 0 9) = [27; 57; 87; 117; 147; 177; 207; 237; 267];
    same_confs_of 4 9 big_omega;
    same_confs_of 5 10 @@ App(no, big_omega);
    same_confs_of 6 11 @@ Lam big_omega;
  ] @ tests (fun t -> term_of_nf @@ eval t);;
