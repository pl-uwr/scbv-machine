(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "00a_nbe.ml";;
*)

(* This is higher-order normalizer with a separate grammar for normal forms. *)

open Tests;;
open Term;;

type level = int
type sem = Abs of (sem -> sem) | Neutral of (level -> neutral)

let rec reify (d : sem) (m : level) : nf =
  match d with
  | Abs f ->
    Nf_lam (reify (f (Neutral (fun m' -> N_var (m'-m-1))))(m+1))
  | Neutral l -> Nf_neutral (l m)

let to_sem (f : sem -> sem) : sem = Abs f

let from_sem (d : sem) : sem -> sem =
  fun d' ->
    match d with
    | Abs f -> f d'
    | Neutral l ->
      Neutral (fun m -> let n = reify d' m in N_app (l m, n))

let rec eval (t : term) (e : sem list) : sem =
  match t with
  | Var n -> List.nth e n
  | Lam t' -> to_sem (fun d -> eval t' (d :: e))
  | App (t1, t2) -> let d2 = eval t2 e
                    in from_sem (eval t1 e) d2

let nbe (t : term) : nf = reify (eval t []) 0

let _ = tests (fun t -> term_of_nf @@ nbe t)
