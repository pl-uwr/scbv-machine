(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "00b.ml";;
*)

(* inlining, renaming  and lvar *)

open Tests;;
open Term;;

type value = Abs of (value -> value) | Neutral of (int -> neutral);;

let lvar (n:int) : value = Neutral(fun m -> N_var(m-n));;

let rec normalize (d : value) (m : level) : nf =
  match d with
  | Neutral l -> Nf_neutral (l m)
  | Abs f     -> Nf_lam (normalize (f (lvar (m+1))) (m+1));;

let apply_value (d : value) (d' : value) : value =
  match d with
  | Abs f -> f d'
  | Neutral l ->
    Neutral (fun m -> let n = normalize d' m in N_app (l m, n));;

type env = value list;;

let rec run (t : term) (e : env) : value =
  match t with
  | Var n -> List.nth e n
  | App (t1, t2) -> let d2 = eval t2 e
                    in apply_value (eval t1 e) d2
  | Lam t' -> Abs (fun d -> eval t' (d :: e));;

let eval (t : term) : nf = normalize (run t []) 0;;

let _ = tests (fun t -> term_of_nf @@ eval t);;
