(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "00a_nbe.ml";;
*)

(* This is a call-by-value variant of a compositional evaluator from
 * the file '07i_nbe.ml' in the 'cbn_deconstruction' directory.  The
 * latter is a NbE (normalization by evaluation), full-reducing
 * evaluator for lambda terms in de Bruijn notation, realizing the
 * normal order reduction strategy.
 * 
 * Here we modify it to a NbE, full-reducing evaluator for lambda
 * terms in de Bruijn notation, realizing the right-to-left full-reducing
 * right-to-left call-by-value reduction strategy. After further
 * transformations described in files '01_inert.ml' -- '08_machine.ml',
 * this evaluator will lead us to an abstract machine implementing
 * a full-reducing (aka strong) call-by-value strategy. *)

open Tests;;
open Term;;

type level = int
type sem = Abs of (sem -> sem) | Neutral of (level -> term)

let rec reify (d : sem) (m : level) : term =
  match d with
  | Abs f ->
    Lam (reify (f (Neutral (fun m' -> Var (m'-m-1))))(m+1))
  | Neutral l ->
    l m

let to_sem (f : sem -> sem) : sem = Abs f

let from_sem (d : sem) : sem -> sem =
  fun d' ->
    match d with
    | Abs f ->
      f d'
    | Neutral l ->
      Neutral (fun m -> let n = reify d' m in App (l m, n))

let rec eval (t : term) (e : sem list) : sem =
  match t with
  | Var n -> List.nth e n
  | Lam t' -> to_sem (fun d -> eval t' (d :: e))
  | App (t1, t2) -> let d2 = eval t2 e
                    in from_sem (eval t1 e) d2

let nbe (t : term) : term = reify (eval t []) 0

(* tests *)
let _ = tests nbe
