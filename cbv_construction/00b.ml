(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "00b.ml";;
*)

(* This is evaluator can be seen as a call-by-value variant of a *
   compositional evaluator from the file '07h.ml' in the
   'cbn_deconstruction' directory.

   Compared to the '00a_nbe.ml', the names of funtions are changed:
    * nbe is renamed to eval
    * eval is renamed to run
    * from_sem is renamed to apply_value
    * reify, after changing the order of arguments, is renamed to normalize
    * to_sem is inlined
    * the inlining of lvar is reversed  *)

open Tests;;
open Term;;

(* The type "name" is changed to "value" *)
type value = Abs of (value -> value) | Neutral of (int -> term);;

(* This function is only adjusted to match the new type. *)
let lvar (n:int) : value = Neutral(fun m -> Var(m-n));;

(* this is not changed *)
let rec normalize (m:int) (v:value) : term =
  match v with
  | Neutral t -> t m
  | Abs f     -> Lam (normalize (m+1) @@ f (lvar (m+1)));;


let apply_value (v:value) (v':value) : value =
  match v with
  | Abs f     -> f v'
  (* In right-to-left strategy we first normalize the argument v',
   * which is made explicit below (in the Neutral case). It was not
   * necessary above (in the Abs case) because Ocaml realizes the same
   * strategy. *)
  | Neutral t -> Neutral(fun m -> let t2 = normalize m v' in App(t m, t2));;

type env = value list;;

let rec run (t:term) (e:env) : value =
  match t with
  | App(t1, t2) -> let v2 = run t2 e in apply_value (run t1 e) v2
  (* Here again we first evaluate t2. *)
  | Lam t       -> Abs(fun v -> run t (v::e))
  | Var n       -> List.nth e n;;

let eval (t:term) : term = normalize 0 @@ run t [];;

let _ = tests eval;;
