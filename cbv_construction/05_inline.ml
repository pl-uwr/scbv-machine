(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "05_inline.ml";;
*)

(* Here the definitions of "apply_wnf", "render_inert" and
 * "normalize"  are inlined in places where they were used in
 * '04_invariant.ml', so that we have only definitions of "run",
 * "continue1" and "continue2". *)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type frame2 = LAPP of inert * int | RAPP of term | LAM;;
type stack2 = frame2 list;;

type stack1 = Lapp of term * env * stack1 | Rapp of wnf * stack1
  | Normalize of int * stack2;;

let rec run (t:term) (e:env) (s1:stack1) : term =
  match t with
  | App(t1, t2) -> run t2 e @@ Lapp(t1,e,s1)                   
  | Lam t       -> continue1 s1 @@ Abs(t, e)
  | Var n       -> continue1 s1 @@ List.nth e n
and continue1 (s1:stack1) (v:wnf) : term =
  match s1 with
  |  Lapp(t1,e, s1) -> run t1 e @@ Rapp(v, s1)
  |  Rapp(v2,   s1) -> (
    match v with
    | Abs(t,e) -> run t (v2::e) s1
    | Inert t  -> continue1 s1 @@ Inert(IApp(t, v2)))
  | Normalize(m,s2) -> 
    match v with
    | Abs(t,e) -> run t (Inert(V(m+1))::e) @@ Normalize (m+1, LAM :: s2)
    | Inert i  -> (
      match i with
      | V(n)       -> continue2 s2 @@ Var(m-n)
      | IApp(i',w) -> continue1 (Normalize (m, (LAPP(i',m) :: s2))) @@ w)
and continue2 (s2:stack2) (t:term) : term =
  match s2 with
  |              [] -> t
  |       LAM :: s2 -> continue2 s2 @@ Lam t
  |   RAPP t2 :: s2 -> continue2 s2 @@ App(t, t2)
  | LAPP(i,m) :: s2 -> continue1 (Normalize(m,RAPP t :: s2)) @@ Inert i;;

let eval (t:term) : term = run t [] (Normalize(0, []));;

let _ = tests eval;;

