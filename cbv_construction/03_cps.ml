(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "03_cps.ml";;
*)


(*    Here we transform the program from '02_closures.ml' to continuation
 *    passing style (cps). Each function below is equipped with
 *    additional parameter "k", an explicit continuation function. Such a
 *    continuation takes care of the further processing of the value
 *    returned by the function in question.
 * 
 *    Continuations are also responsible for exposing an explicit order
 *    of computation --- all intermediate values are explicitly passed to
 *    corresponding continuations. Consider the "run" function from 
 *    '02_closures.ml':
 *       "let rec run (t:term) (e:env) : wnf"
 *    becomes now
 *       "let rec run (t:term) (e:env) (k:wnf -> 'a) : 'a".
 *    The continuation "k" will process the result (of type "wnf") of the
 *    function "run"; we do not know how it is going to do it, hence
 *    we equip it with the type  "wnf -> 'a". Consider the first
 *    clause of the pattern matching in the definition of this function:
 *     "| App(t1, t2) -> 
 *           let v2 = run t2 e in apply_wnf (run t1 e) v2".
 *    Here, we start by computing the value of "run t2 e". Then this
 *    value is passed to a continuation that names it "v2", computes the
 *    next intermediate value, namely "run t1 e" and passes it to the
 *    second continuation. The second continuation names the obtained
 *    value "v1", computes "apply_wnf v1 v2" and passes this value to the
 *    continuation "k".
 * 
 *    Similar transformations are made to all functions and all
 *    intermediate values in '02_closures.ml'.
 * 
 *    Function "eval" is of particular interest: it is the main function
 *    here, so, after computing the last intermediate value, it should
 *    stop. This is done by calling the identity function as the topmost
 *    continuation. In effect, this continuation stops and simply
 *    returns the result. *)
open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

let rec run (t:term) (e:env) (k:wnf -> 'a) : 'a =
  match t with
  | App(t1, t2) -> run t2 e (fun v2 ->
                   run t1 e (fun v1 ->
                   apply_wnf v1 v2 k))
  | Lam t       -> k @@ Abs(t, e)
  | Var n       -> k @@ List.nth e n
and apply_wnf (v:wnf) (v': wnf) (k:wnf -> 'a) : 'a =
  match v with
  | Abs(t,e) -> run t (v'::e) k
  | Inert t  -> k @@ Inert(IApp(t, v'));;

let rec render_inert (i:inert) (m:int) (k:term -> 'a) : 'a =
  match i with
  | V(n)       -> k @@ Var(m-n)
  | IApp(i',w) -> normalize m w (fun t2 -> 
                  render_inert i' m (fun t1 ->
                  k @@ App(t1, t2)))
and normalize (m:int) (v:wnf) (k:term -> 'a) : 'a =
  match v with
  | Inert t  -> render_inert t m k
  | Abs(t,e) -> run t (Inert(V(m+1))::e) (fun v ->
                normalize (m+1) v (fun t -> k @@ Lam t));;

let eval (t:term) : term = run t [] (fun v -> normalize 0 v (fun t -> t));;

let _ = tests eval;;

