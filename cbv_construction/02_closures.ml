(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "02_closures.ml";;
*)

(*    Here we defunctionalize the function used in the 
 *      "Abs of (value -> value)"
 *    variant of the type "value" defined in '01_inert.ml'.
 * 
 *    Again, we start by identifying places where the function is
 *    constructed. The only such place is the second clause of pattern
 *    matching in the definition of the "run" function, containing
 *    "Abs(fun v -> run t (v::e))". There are two free variables, "t" of
 *    type "term" and "e" of type "env". This leads to a new type 
 *    "t = T of term * env" with just one variant, and 
 *    "value = t | Inert of inert" which is not optimal. We optimize it
 *    by recycling the constructor "Abs". Now we observe that the obtained
 *    type is isomorphic to (closures of) lambda terms in weak normal
 *    form, with a syntactic subcategory of inert terms, so we rename
 *    it accordingly, obtaining 
 *       "wnf = Abs of term * env | Inert of inert".
 *    Now "Abs(t,e)" represents the function "fun v -> run t (v::e)"
 *    and we replace the construction of this function by an injection
 *    to the new type:
 *       "| Lam t -> Abs(t, e)".
 * 
 *    Again, the second step is to define the interpreting function which
 *    could be something like 
 *      "let render_abs (t:term) (e:env) (v: wnf) : wnf =
 *         run t (v::e);;"
 * 
 *    And again, we identify the places where the defunctionalized function is 
 *    used. These are "f (Inert(V(m+1)))" in the second clause in the
 *    definition of "normalize" and "f v'" in the first clause in the
 *    definition of "apply_value". This leads to 
 *      "| Abs(t,e)    -> Lam (normalize (m+1) @@ render_abs t e (Inert(V(m+1))));;"
 *    and 
 *      "| Abs(t,e)   ->  render_abs t e v';;",
 *    but "render_abs" is so simple that we can inline it, obtaining more
 *    elegant code.  *)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;


let rec run (t:term) (e:env) : wnf =
  match t with
  | App(t1, t2) -> let v2 = run t2 e in apply_wnf (run t1 e) v2
  | Lam t       -> Abs(t, e)
  | Var n       -> List.nth e n
and apply_wnf (v:wnf) (v':wnf) : wnf =
  match v with
  | Abs(t,e) -> run t (v'::e)
  | Inert t  -> Inert(IApp(t, v'));;

let rec render_inert (i:inert) (m:int) : term =
  match i with
  | V(n)       -> Var(m-n)
  | IApp(i',w) -> let t2 = normalize m w in App(render_inert i' m, t2)
and normalize (m:int) (v:wnf) : term =
  match v with
  | Inert t  -> render_inert t m
  | Abs(t,e) -> Lam (normalize (m+1) @@ run t (Inert(V(m+1))::e));;

let eval (t:term) : term = normalize 0 @@ run t [];;

let _ = tests eval;;

