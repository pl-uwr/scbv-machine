(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "04_invariant.ml";;
*)

open Tests;;
open Term;;

(*    The cps transformation has introduced several new functions in 
 *    '03_cps.ml', which we now must defunctionalize.
 * 
 *    Since the topmost continuation in '03_cps.ml' is the identity
 *    function of type (term -> term), the resulting type "'a" gets
 *    instantiated to "term" everywhere. Hence, there are occurrences of
 *    two types of continuations: "wnf -> term" and "term -> term", which
 *    lead to two new data types: "stack1" and "stack2".
 * 
 *    There are four places of construction of a function of the first
 *    type: two in the definition of "run", one in the definition of
 *    "render_inert" and one in "eval"; the last two of them have the
 *    same shape and are treated as one. This leads to the three variants
 *    of type "stack1" below. Now "Lapp(t1,e,s)" represents the function
 *    "fun v2 -> run t1 e (fun v1 -> apply_wnf v1 v2 k)))k" where "k" is
 *    the continuation function represented by "s"; "Rapp(v2,s)"
 *    represents the function "fun v1 -> apply_wnf v1 v2 k" and
 *    "Normalize(m,s)" represents "fun v -> normalize m v k" where "k" is
 *    the continuation function represented by "s". Again, in all four
 *    places the construction of these functions is replaced by an
 *    injection to the new type.
 * 
 *    The function interpreting the type "stack1" is "continue1".  For
 *    each variant of the new type, it calls the represented function.
 *    Then each use of a continuation function of type "wnf -> term" is
 *    replaced by a call to "continue1".
 * 
 *    Similarly, there are four places of construction of continuations
 *    of the second type ("term -> term"): two in the definition of
 *    "render_inert", one in "normalize" and one in "eval". This leads to
 *    four variants of type "stack2": 
 *       "LAPP of inert * int * stack2 | RAPP of term * stack2 
 *          | LAM of stack2 | [] ;;".
 *    With this definition "LAPP(i',m,s)" represents the function 
 *      "fun t2 -> render_inert i' m (fun t1 ->
 *         k @@ App(t1, t2))" where "k" is represented by "s",
 *    "RAPP(t2,s)" represents 
 *       "fun t1 -> k @@ App(t1, t2))" 
 *    and "LAM(s)" represents  "fun t -> k @@ Lam t" where "k" is 
 *    represented by "s", and "[]" represents "fun t -> t". 
 * 
 *    However, to underline the stack structure, we split the definition
 *    into two: the definition of the stack implemented as a list of
 *    elements stored on the stack (called "frames"), and the definition
 *    of the type of frames. This gives the definition below. The
 *    function "continue2", which is the interpreting function for the
 *    new type, is changed accordingly. Finally, each use of a
 *    continuation function of type "term -> term" is replaced by a call
 *    to "continue2".  
 *    
 *    The name of the file, "invariant", comes from the corresponding
 *    stage of the deconstruction of the KN machine. *)

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type frame2 = LAPP of inert * int | RAPP of term | LAM;;
type stack2 = frame2 list;;

type stack1 = Lapp of term * env * stack1 | Rapp of wnf * stack1
  | Normalize of int * stack2;;

let rec run (t:term) (e:env) (s1:stack1) : term =
  match t with
  | App(t1, t2) -> run t2 e @@ Lapp(t1,e,s1)                   
  | Lam t       -> continue1 s1 @@ Abs(t, e)
  | Var n       -> continue1 s1 @@ List.nth e n
and apply_wnf (v:wnf) (v': wnf) (s1:stack1) : term =
  match v with
  | Abs(t,e) -> run t (v'::e) s1
  | Inert t  -> continue1 s1 @@ Inert(IApp(t, v'))
and render_inert (i:inert) (m:int) (s2:stack2) : term =
  match i with
  | V(n)       -> continue2 s2 @@ Var(m-n)
  | IApp(i',w) -> normalize m w (LAPP(i',m) :: s2)
and normalize (m:int) (v:wnf) (s2:stack2) : term =
  match v with
  | Inert t  -> render_inert t m s2
  | Abs(t,e) -> run t (Inert(V(m+1))::e) @@ Normalize (m+1, LAM :: s2)
and continue1 (s1:stack1) (v:wnf) : term =
  match s1 with
  |  Lapp(t1,e, s1) -> run t1 e @@ Rapp(v, s1)
  |  Rapp(v2,   s1) -> apply_wnf v v2 s1
  | Normalize(m,s2) -> normalize m v s2
and continue2 (s2:stack2) (t:term) : term =
  match s2 with
  |              [] -> t
  |       LAM :: s2 -> continue2 s2 @@ Lam t
  |   RAPP t2 :: s2 -> continue2 s2 @@ App(t, t2)
  | LAPP(i,m) :: s2 -> render_inert i m (RAPP t :: s2);;

let eval (t:term) : term = run t [] (Normalize(0, []));;

let _ = tests eval;;

