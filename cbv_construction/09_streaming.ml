(*
#directory "../common";;
#load_rec "machine.cmo";;
#use "09_streaming.ml";;
*)

(* This file is not a transformation of a code of '08_machine.ml'.
 * Here we use the constructed machine to perform normal forms comparison via
 * term streaming. *)

(* We show how to solve the following problem: given two terms t1 and t2, 
   is it true that t1 and t2 have the same normal form? *) 

(* Recall that the machine from "08_machine.ml" computes, for a given
   term t its strong normal form. *)

(* The idea is to run the machine on both terms as long as partial
   results are the same. If the machine completes the computation on
   both terms and the computed normal forms are equal, we have a
   positive answer to the problem. But whenever it sees partial
   results that are different for the two input terms, we have a
   negative answer without actually completing the computation of the
   normal forms.  In some cases it allows to give a negative answer
   even on divergent terms (see example below) *)  

open Term;;
open Machine;;

(* When the machine is in one of the two configurations below, we are
   able to construct a part of the resulting normal form. *)
let partial_result (c:conf) : (frame * conf) option =
  match c with
  | E(t, e, LAM::s,   m) -> Some (    LAM, E(t, e, s, m))
  | C(RAPP t2::s, t1, m) -> Some (RAPP t2, C(s, t1, m))
  |                    _ -> None;;

(* This function is used to run the machine until a configuration with
   a partial result is reached. *) 
let rec to_partial_result (c:conf) : frame option * conf =
  match partial_result c with
  | Some(f,c') -> (Some f, c')
  | None ->
  match trans c with
  | Some c' -> to_partial_result c'
  | None ->
  (None, c);;


(* An auxillary function for comparing normal forms of terms. Partial
   result of the form "(None, c)" means that the computation is
   finished and it is enough to compare the computer normal
   forms. Otherwise we compare the computer partial results; if they
   are equal, then the computation is resumed with partial results
   removed (they are equal, so there will be no need to compare them
   again).*)
let rec compare_nf_aux (c1:conf) (c2:conf) : bool =
  let (f1, c1') = to_partial_result c1 in
  let (f2, c2') = to_partial_result c2 in
  if f1 <> f2  then false                   else
  if f1 = None then unload c1' = unload c2' else
  compare_nf_aux c1' c2';;

let compare_nf (t1:term) (t2:term) : bool = compare_nf_aux (load t1) (load t2);;


(* t1 and t2 are diverging terms. Even if the evaluation of these
   terms never terminates, we detect different partial resuts and can
   answer that these two terms do not have equal normal forms.*) 
let t1 = Lam(          Lam(             big_omega));;
let t2 = Lam(App(App(Var 0, Lam big_omega), Var 0));;

let t3 = Lam(App(App(omega, Var 0), App(omega, Var 0)));;
let t4 = Lam(App(omega,             App(Var 0, Var 0)));;


(* t1 t2 - terms from paper *)
let tests : bool list = [
  compare_nf id (App(App(church 10, omega), id)) = true;
  compare_nf t1 t2 = false;
  compare_nf t3 t4 = true
];;

