(*
#directory "../common";;
#load_rec "tests.cmo";;
#load "../common/trampoline.cmo";;
#use "08_machine.ml";;
*)

(*   Here we change the functional notation used in '07_entanglement.ml'
 *   to a term-rewriting one. There are three variants of
 *   configurations:
 *     "E(t,e,s,m)" corresponds to a call of "run t e m s",
 *     "C(s,w,m)"   corresponds to a call of "continue1 w m s",
 *     "S(s,t,m)"   corresponds to a call of "continue2 t m s". *)


open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type frame = LAPP of inert | RAPP of term | LAM
           | Lapp of  term * env | Rapp of wnf;;
type stack = frame list;;

type conf = E of term * env * stack * int
  | C of stack * wnf * int | S of stack * term * int;;

let trans (c:conf) : conf option =
  match c with
  | E(Var _, [], _, _)    -> None
  | S(Lapp(_,_)::_, _, _) -> None
  | S(Rapp _   ::_, _, _) -> None
  | S([], t, _)           -> None
  | _ -> Some(
  match c with
  | E(App(t1, t2), e, s1, m) -> E(t2, e, Lapp(t1, e)::s1, m)
  | E(Lam t, e, s1, m)       -> C(s1, Abs(t,e), m)
  | E(Var 0, w::_, s1, m)    -> C(s1, w, m)
  | E(Var n, _::e, s1, m)    -> E(Var(n-1), e, s1, m)
  | C(Lapp(t, e)::s1, w, m)    -> E(t, e, Rapp w::s1, m)
  | C(Rapp w::s1, Abs(t,e), m) -> E(t, w::e, s1, m)
  | C(Rapp w::s1, Inert i,  m) -> C(s1, Inert(IApp(i, w)), m)
  | C(s3, Abs(t,e), m)         -> E(t, Inert(V(m+1))::e, LAM::s3, m+1)
  | C(s2, Inert(IApp(i,w)), m) -> C(LAPP i::s2, w, m)
  | C(s2, Inert(V(n)), m)      -> S(s2, Var(m-n), m)
  | S(LAPP i::s2, t_nf, m)     -> C(RAPP t_nf::s2, Inert i, m)
  | S(LAM::s3, t_nf, m)        -> S(s3, Lam t_nf, m-1)
  | S(RAPP t_nf::s2, t_neu, m) -> S(s2, App(t_neu, t_nf), m)
  | E(Var _, [], _, _)    -> assert false
  | S(Lapp(_,_)::_, _, _) -> assert false
  | S(Rapp _   ::_, _, _) -> assert false
  | S([], t, _)           -> assert false
  );;

let load (t:term) : conf = E(t, [], [], 0);;

let unload (c:conf) : term =
  match c with
  | S([], t, _) -> t
  | _           -> assert false;;

open Trampoline;;

let nf_trampoline (t:term) : term trampolined = trampolined_map unload @@ opt_trampoline trans @@ load t;;

let steps_for_family (ts:int -> term) (n:int) : int = count_bounces @@ nf_trampoline @@ ts n;;

let eval (t:term) : term = pogo_stick @@ nf_trampoline t;;

let ith_conf_of (i:int) (t:term) : conf = iter i (opt_try_step trans) (load t);;

let same_confs_of (i:int) (j:int) (t:term) : bool = (ith_conf_of i t = ith_conf_of j t);;

let _ = [
  List.map (steps_for_family test1) @@ (ints 0 5 @ [9]) = [15; 33; 63; 117; 219; 6201];
  List.map (steps_for_family explode) @@ (ints 0 5 @ [9]) = [15; 29; 47; 73; 115; 2149];
  List.map (steps_for_family contained_explosion) @@ (ints 0 9) = [25; 55; 85; 115; 145; 175; 205; 235; 265];
  same_confs_of 4 9 big_omega;
  same_confs_of 5 10 @@ App(no, big_omega);
  same_confs_of 6 11 @@ Lam big_omega;
] @ tests eval;;
