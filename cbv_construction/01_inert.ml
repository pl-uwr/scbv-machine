(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "01_inert.ml";;
*)

(* The first transformation is defunctionalization of the function
 * used in the "Neutral of (int -> term)" variant of the type "value"
 * defined in '00b.ml'.
 * 
 * First, we identify places in '00b.ml' where this function is
 * constructed (i.e., defined, or introduced). This is 
 *    "Neutral(fun m -> Var(m-n))" 
 * in the definition of "lvar" function, and 
 *    "Neutral(fun m -> let t2 = normalize m v' in App(t m, t2))" 
 * in the definition of "apply_value". In the former one,  the only free
 * variable is "n" of type "int", which leads to the variant 
 * "V of int" of the new type "inert". Now "V(n)" represents the function 
 * "fun m -> Var(m-n)", so this function is replaced by an injection to the
 *  new type: 
 *    "let lvar (n:int) : value = Inert(V(n));;"
 * but this function short and only once used, so we inline it in
 *    "| Abs f   -> Lam (normalize (m+1) @@ f (Inert(V(m+1))));;".
 * In the latter place, there are two free variables:
 * "t" of the type being currently defunctionalized, and "v'" of type 
 * "value" --- this leads to the second variant, where "IApp(t,v')" represents 
 * the function "fun m -> let t2 = normalize m v' in App(t m, t2)". This 
 * way the new data type enumerates all lambda abstractions from 
 * '00_nbe.ml' that correspond to the type  "Neutral of (int -> term)".
 * Again, we inject represented function to the new type:
 *    "| Inert t -> Inert(IApp(t, v'));;".
 * 
 * The choice of the name of the new type ("inert") will be discussed 
 * in the next transformation step.
 *
 * Second, we define the function "render_inert" meant to interpret
 * the new type by dispatching the corresponding functions:
 * "let render_inert (i:inert) (m:int) : term =
 *    match i with
 *    | V(n)       -> Var(m-n)
 *    | IApp(t,v') -> let t2 = normalize m v' in App(t m, t2)".
 * Note that this function still needs to be modified, because the 
 * occurrence of "t" in "IApp(t,v')" is of new type while the occurrence
 * in "App(t m, t2)" is of old type.
 * 
 * Third, we identify the places where the defunctionalized function is 
 * used (i.e., deconstructed or eliminated). These are "t m" in the first 
 * clause of the pattern matching in the definition of "normalize", and 
 *   "let t2 = normalize m v' in App(t m, t2)"
 * in the second clause of the pattern matching in the definition 
 * of "apply_value" (which, at the current stage of the transformation, is
 *  moved to "render_inert" above). Having identified this, we just replace 
 * these uses by calls to "render_inert". This makes "render_inert" and 
 * "normalize" mutually recursive. *)

open Tests;;
open Term;;

type value = Abs of (value -> value) | Inert of inert
and inert = V of int | IApp of inert * value;;

let rec render_inert (i:inert) (m:int) : term =
  match i with
  | V(n)       -> Var(m-n)
  | IApp(i',w) -> let t2 = normalize m w in App(render_inert i' m, t2)
and normalize (m:int) (v:value) : term =
  match v with
  | Inert t -> render_inert t m
  | Abs f   -> Lam (normalize (m+1) @@ f (Inert(V(m+1))));;

let apply_value (v:value) (v':value) : value =
  match v with
  | Abs f   -> f v'
  | Inert t -> Inert(IApp(t, v'));;

type env = value list;;

let rec run (t:term) (e:env) : value =
  match t with
  | App(t1, t2) -> let v2 = run t2 e in apply_value (run t1 e) v2
  | Lam t       -> Abs(fun v -> run t (v::e))
  | Var n       -> List.nth e n;;

let eval (t:term) : term = normalize 0 @@ run t [];;

let _ = tests eval;;

