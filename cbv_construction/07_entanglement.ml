(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "07_entanglement.ml";;
*)

(* Here we entangle nested pattern matching in the definition of
   "continue1":

     "match s with [...]  
      | match v with [...]"  

   is replaced with 

     "match v,s with [...]"

 *)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type frame = LAPP of inert | RAPP of term | LAM
           | Lapp of term * env | Rapp of wnf;;
type stack = frame list;;

let rec run (t:term) (e:env) (m:int) (s:stack) : term =
  match t with
  | App(t1, t2) -> run t2 e m @@ Lapp(t1,e)::s                   
  | Lam t       -> continue1 (Abs(t, e)) m s
  | Var n       -> continue1 (List.nth e n) m s
and continue1 (v:wnf) (m:int) (s:stack) : term =
  match v, s with
  |        v, Lapp(t1,e)::s -> run t1 e m @@ Rapp v::s
  | Abs(t,e), Rapp v2   ::s -> run t (v2::e) m s
  | Abs(t,e),             _ -> run t (Inert(V(m+1))::e) (m+1) @@ LAM::s
  | Inert t,  Rapp v2   ::s -> continue1 (Inert(IApp(t, v2))) m s
  | Inert (V(n)),         _ -> continue2 (Var(m-n)) m s 
  | Inert (IApp(i',w)),   _ -> continue1 w m (LAPP i'::s)
and continue2 (t:term) (m:int) (s:stack) : term =
  match s with
  |         [] -> t
  |     LAM::s -> continue2  (Lam t) (m-1) s
  | RAPP t2::s -> continue2  (App(t, t2)) m s
  | LAPP i1::s -> continue1  (Inert i1) m (RAPP t::s)
  | Lapp _ ::s -> assert false
  | Rapp _ ::s -> assert false;;

let eval (t:term) : term = run t [] 0 [];;

let _ = tests eval;;

