(*
#directory "../common";;
#load_rec "tests.cmo";;
#use "06_counter.ml";;
*)

(*    There are several transformations here, they do not have a
 *    corresponding step in the deconstruction of KN machine.
 * 
 *    First, the two stacks are flattened into one. The new type "frame"
 *    is the union of frames that could occur on either stack in
 *    '05_inline.ml'. The information about the number of the stack was
 *    stored in two places: in the type of the stack and in the
 *    definition of the continue function. One of them is enough.
 * 
 *    Second, the integer "m" in "LAPP(i,m)" and in "Normalize(m,s2)"
 *    does not change arbitrarily. It stores the nesting level of lambda
 *    abstractions processed so far, so it is enough to introduce a new
 *    counter and to keep its value as a parameter of the current
 *    function --- there is no need to keep it on stack.
 * 
 *    Third, after changing "Normalize(m,s2)" to "Normalize(m)::s2" and
 *    removing "m", "Normalize" becomes a marker on stack --- it marks
 *    places where the type of stack changes from what was before
 *    "stack1" to "stack2"; in other words, places where "continue1"
 *    calls "continue2". This is no longer necessary and we can get rid
 *    of the constructor "Normalize".
 * 
 *    The "false" assertions in the definition of "continue2" express the
 *    invariant that it is not possible to encounter frames "Lapp" or
 *    "Rapp" while processing a stack of former type "stack2". They are
 *    use to prevent OCaml interpreter from displaying warnings about
 *    non-exhaustive pattern matching.   *)

open Tests;;
open Term;;

type wnf = Abs of term * env | Inert of inert
and inert = V of int | IApp of inert * wnf
and env = wnf list;;

type frame = LAPP of inert      | RAPP of term | LAM
           | Lapp of term * env | Rapp of wnf;;
type stack = frame list;;

type value = Wnf of wnf | Normal of term;;

let rec run (t:term) (e:env) (m:int) (s:stack) : term =
  match t with
  | App(t1, t2) -> run t2 e m @@ Lapp(t1,e) :: s                   
  | Lam t       -> continue1 m s @@ Abs(t, e)
  | Var n       -> continue1 m s @@ List.nth e n
and continue1 (m:int) (s:stack) (v:wnf) : term =
  match s with
  | Lapp(t1,e) :: s -> run t1 e m @@ Rapp v :: s
  | Rapp v2    :: s -> (
    match v with
    | Abs(t,e) -> run t (v2::e) m s
    | Inert t  -> continue1 m s @@ Inert(IApp(t, v2)))
  | _ ->
    match v with
    | Abs(t,e) -> run t (Inert(V(m+1))::e) (m+1) @@ LAM :: s
    | Inert i  ->
      match i with
      | V(n)       -> continue2 m s @@ Var(m-n)
      | IApp(i',w) -> continue1 m (LAPP i' :: s) w
and continue2 (m:int) (s:stack) (t:term) : term =
    match s with
    |           [] -> t
    |     LAM :: s -> continue2 (m-1) s @@ Lam t
    | RAPP t2 :: s -> continue2 m s @@ App(t, t2)
    |  LAPP i :: s -> continue1 m (RAPP t :: s) @@ Inert i
    |  Lapp _ :: s -> assert false
    |  Rapp _ :: s -> assert false;;

let eval (t:term) : term = run t [] 0 [];;

let _ = tests eval;;

